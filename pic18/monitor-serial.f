load boot.f         \ init code
load prom.f         \ flash programming driver
load serial.f       \ serial port driver
load interpreter.f  \ command interpreter
load route.f        \ support for the 'route' dispatch macro
load string.f       \ support for flash buffers

\ load mem.f          \ generic fetch


\ these need to be procedures so they can be overridden by either
\ procedures or macros. (currently macros cannot be overridden by
\ procedures). FIXME: solve redirect inefficiency in asm optimization.

: receive    async.rx> ;
: transmit   async.>tx ;
: console-on ;
: console-off ;

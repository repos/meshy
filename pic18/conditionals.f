\ standard forth conditionals not using the flags. these are less
\ efficient than the flag macros, but easier to use. best to use these
\ if code is not time critical.

: >z   0 + nfdrop ;  \ used in 'cbra' macro as fallback

macro
: ~cmp  y n i |
    POSTDEC0 i compile
    n retlw
    y retlw ;
forth  

: =  -1  0 ' cpfseq ~cmp
: >  -1  0 ' cpfsgt ~cmp
: <  -1  0 ' cpfslt ~cmp
: <=  0 -1 ' cpfsgt ~cmp
: >=  0 -1 ' cpfslt ~cmp
    

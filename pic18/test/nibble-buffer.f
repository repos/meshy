load nibble-buffer.f

variable buf-rw

macro
: buf   buf-rw #x100 ;
: buf?  buf buffer.empty? not ;  
forth

: >buf buf buffer.write ;
: buf> buf buffer.read ;

: clear-buf 0 buf-rw ! ;
: buf-size  buf buffer.size ;

: dump-buf
    begin
	buf? while
	    buf>
    repeat ;
    
\ convert access bank address to real address in ah:al
: access>a
    dup al !
    #x80 xor rot<<c
    drop ah @ ah --! ;
    
: @ access>a @a+ ;
: ! access>a !a+ ;

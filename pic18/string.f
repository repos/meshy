\ -*- forth -*-

\ loads the address following the call to 'f->' into f register, and
\ returns to the parent word. this allows flash buffers/strings to be
\ implemented as ordinary words

\ : mystring  f->  0 , 1 , 2 , 

: f-> \ -- 
    rl @ fl !
    rh @ fh !
    pop ;

\ a string is an fobject with the first byte = size of the remaining
\ bytes.

    
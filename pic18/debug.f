\ debug routines

load cm.f \ use compiled macros


\ wait, but reset when bytes are arriving on the port

: rint  receive interpret ;

: pause 0 for 0 for
	    rx-ready? if reset then
	next next ;

    
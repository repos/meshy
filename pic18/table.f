\ single lookup table: works like 'route' but returns a byte

: table       \ index
    rl +!     \ add offset to return address
    0 rh ++!  \ add carry flag
    
    rl @ fl !  \ r>f  NOTE: this messes up the VM
    rh @ fh !

    pop
    @f+ ;


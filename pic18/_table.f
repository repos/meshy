\ double lookup table: works like 'route' but returns 2 bytes.

: _table       \ index
    rot<<      \ word addressing (hide MSB in LSB)
    dup
    rl +!      \ add offset to return address
    1 and      \ recover high bit
    rh ++!     \ add carry flag and
    
    rl 0 low   \ clear the hidden MSB
    
    rl @ fl ! 
    rh @ fh !

    pop
    @f+ @f+ ;

    

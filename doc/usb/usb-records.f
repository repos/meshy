( -*- forth -*- )

( This file contains all maps to interpret usb data records
  [name->number] and request handlers [number->name], while the logic
  corresponding logic is in usb.f )

( Buffer Descriptor Table

  OUT/SETUP packets are packets coming from the host. The
  corresponding buffers are owned by the SIE.

  IN packets are packets going to the host. The corresponding buffers
  are owned by the core. )
  
( The USTAT register can be used as a direct index into the BDT, when
  pingpong buffering is not used. otherwize, it needs to be shifted by
  one. Contents of USTAT are

  [7]    -
  [6:3]  endpoint  0x00 - 0x0F
  [2]    direction 0 = OUT/SETUP   1 = IN
  [1]    pingpong buffer index
  [0]    - )

( pic 18f buffer descriptors )
struct BD
( 0 ) status
( 1 ) size
( 2 ) addr-lo addr-hi
end


( The bd-status register, called BDnSTAT in the datasheet, contains
  the following data when owned by the SIE.

  [1:0]  high bits of buffer size

  [5:2]  packet PID

  Packet Identifier Fields are part of the Protocol Layer described in
  the USB1.1 spec, 8.3.1 page 156. Most of this is handled by the SIE
  hardware. The ones that need to be handled in software are

  0x1 OUT
  0x3 IN
  0xD SETUP

  [7]    ownership

  0 = owned by the core
  1 = owned by the SIR )

router TOKEN_PID
( 0x0 )   reserved
( 0x1 ) TOKEN_OUT
( 0x2 )   reserved
( 0x3 ) TOKEN_IN
( 0x4 )   reserved
( 0x5 )   reserved
( 0x6 )   reserved
( 0x7 )   reserved
( 0x8 )   reserved
( 0x9 )   reserved
( 0xa )   reserved
( 0xb )   reserved
( 0xc )   reserved
( 0xd ) TOKEN_SETUP
( 0xe )   reserved
( 0xf )   reserved
end


( SETUP record is 8 bytes
  USB1.1 spec, 9.3 page 183 )
struct SETUP
( 0 ) request-type
( 1 ) request
( 2 ) value-lo  value-hi
( 4 ) index-lo  index-hi   ( endpoint 3:2, direction 7 )
( 6 ) length-lo length-hi
end

( setup-request-type )

(  direction   [7]

  0x0 host->device [OUT]
  0x1 device->host [IN] )
    

( type        [6:5] )

router REQUEST_TYPE
( 0x0 ) STANDARD_REQUEST
( 0x1 ) CLASS_REQUEST
( 0x2 ) VENDOR_REQUEST
( 0x3 ) reserved
end
  
( recipient   [4:0]

  0x00 DEVICE
  0x01 INTERFACE
  0x02 ENDPOINT
  0x03 OTHER
  0x04-0x1F reserved )


( STANDARD requests
  USB1.1 spec, 9.3 page 186 )


router STANDARD_REQUEST
( 0x00 ) GET_STATUS
( 0x01 ) CLEAR_FEATURE
( 0x02 ) reserved
( 0x03 ) SET_FEATURE
( 0x04 ) reserved
( 0x05 ) SET_ADDRESS
( 0x06 ) GET_DESCRIPTOR
( 0x07 ) SET_DESCRIPTOR
( 0x08 ) GET_CONFIGURATION
( 0x09 ) SET_CONFIGURATION
( 0x0A ) GET_INTERFACE
( 0x0B ) SET_INTERFACE
( 0x0C ) SYNCH_FRAME
( 0x0D ) reserved
( 0x0E ) reserved
( 0x0F ) reserved
end

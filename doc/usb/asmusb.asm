		list p=18f2550		;list directive to define processor
		#include <p18f2550.inc>	;processor specific definitions

	CONFIG	PLLDIV = 2
	CONFIG 	CPUDIV = OSC1_PLL2
	CONFIG 	USBDIV = 2
	CONFIG 	FOSC = HSPLL_HS
	CONFIG	PWRT = ON
	CONFIG	VREGEN = ON	
	CONFIG	WDT = OFF
	CONFIG	LVP = OFF
		
		; WDT OFF
		; LVP OFF
		
;	CONFIG	_CONFIG1H, _FOSC_HSPLL_HS_1H 
;	CONFIG	_CONFIG2L, _PWRT_ON_2L & _VREGEN_ON_2L
;	CONFIG	_CONFIG2H, _WDT_OFF_2H
;	__CONFIG	_CONFIG3H, _CCP2MX_OFF_3H
;	__CONFIG	_CONFIG4L, _STVR_OFF_4L & _LVP_OFF_4L & _DEBUG_OFF_4L
;	__CONFIG	_CONFIG5L, _CP0_OFF_5L & _CP1_OFF_5L & _CP2_OFF_5L & _CP3_OFF_5L 
;	__CONFIG	_CONFIG5H, _CPB_OFF_5H & _CPD_OFF_5H
;	__CONFIG	_CONFIG6L, _WRT0_OFF_6L & _WRT1_OFF_6L & _WRT2_OFF_6L & _WRT3_OFF_6L 
;	__CONFIG	_CONFIG6H, _WRTC_OFF_6H & _WRTB_OFF_6H & _WRTD_OFF_6H
;	__CONFIG	_CONFIG7L, _EBTR0_OFF_7L & _EBTR1_OFF_7L & _EBTR2_OFF_7L & _EBTR3_OFF_7L
;	__CONFIG	_CONFIG7H, _EBTRB_OFF_7H
 

; the dissassembler is a little too greedy in re-assigning symbols
#define buffer 0

		CBLOCK 0x00
			baudconfig
			res001
			__tmp_0
		ENDC
		CBLOCK	0x060
			old_sw2 		; 0x60
			old_sw3 		; 0x61
			input_buffer:32 	; 0x62
			output_buffer:32 	; 0x62
		ENDC
		CBLOCK	0xA2
			cdc_rx_len		;0x0A2
			cdc_trf_state
			pCDCSrc
			pCDCSrcMSB   
			pCDCDst
			pCDCDstMSB   
			cdc_tx_len
			cdc_mem_type
			line_coding		; 0x0AA
		ENDC
		CBLOCK 0x0B1
	 		control_signal_bitmap
		ENDC
		CBLOCK 0x00B5   
		   dummy_encapsulated_cmd_response
		ENDC
		CBLOCK    0x0BD
			prom
		ENDC
		CBLOCK 0x0C0  
			curr_byte
			res100
			curr_entry
			res101
			data_ptr
			res102
			res103
			ctrl_trf_state
			ctrl_trf_session_owner
			pSrc
			res104
			pDst
			res105
			wCount
			res106
			usb_device_state
			usb_stat
			usb_active_cfg
			usb_alt_intf
			delay_count
			res107
			led_count
		ENDC		
		
	org   0x0000               
_entry    GOTO _startup   
___return_lbl00000 RETURN 0   
		NOP   
		NOP   
		NOP   
		NOP   
		NOP   
		NOP   
		NOP   
		NOP   
		NOP   
		NOP   
		NOP   
		NOP   
		NOP   
		NOP   
		NOP   
		NOP   
		NOP   
		NOP   
		dw 0x0001
		dw 0x0dcc   
		dw 0x0000
 				  dw 0x00d5 
				  dw 0x0000
				  dw 0x0002
				  dw 0x0000   
USBCheckCDCRequest MOVLB 0x4   
		MOVF 0x20, W, BANKED   
		ANDLW 0x1f   
		SUBLW 0x1   
		BZ 0x44   
		BRA 0xfa   
		MOVF 0x20, W, BANKED   
		ANDLW 0x60   
		RRNCF WREG, F, ACCESS   
		RRNCF WREG, F, ACCESS   
		RRNCF WREG, F, ACCESS   
		RRNCF WREG, F, ACCESS   
		RRNCF WREG, F, ACCESS   
		SUBLW 0x1   
		BZ 0x58   
		BRA 0xfa   
		MOVF 0x24, W, BANKED   
		BZ 0x62   
		DECF 0x24, W, BANKED   
		BZ 0x62   
		BRA 0xfa   
		MOVF 0x21, W, BANKED   
		XORLW 0x23   
		BZ 0xf8   
		XORLW 0x1   
		BZ 0xec   
		XORLW 0x3   
		BZ 0xd0   
		XORLW 0x1   
		BZ 0xbe   
		XORLW 0x24   
		BZ 0xbc   
		XORLW 0x7   
		BZ 0xba   
		XORLW 0x1   
		BZ 0xb8   
		XORLW 0x3   
		BZ 0xa6   
		XORLW 0x1   
		BZ 0x8a   
		BRA 0xfa   
		MOVLB 0   
		MOVLW 0x3   
		MOVWF ctrl_trf_session_owner, BANKED  
		MOVLB 0   
		MOVLW 0xb5   
		MOVWF pSrc, BANKED   
		MOVLW 0   
		MOVWF 0xca, BANKED   
		MOVLB 0   
		BCF usb_stat, 0x1, BANKED   
		MOVLB 0   
		MOVLW 0x8   
		MOVWF wCount, BANKED   
		BRA 0xfa   
		MOVLB 0   
		MOVLW 0x3   
		MOVWF ctrl_trf_session_owner, BANKED  
		MOVLB 0   
		MOVLW 0xb5   
		MOVWF pDst, BANKED   
		MOVLW 0   
		MOVWF 0xcc, BANKED   
		BRA 0xfa   
		BRA 0xfa   
		BRA 0xfa   
		BRA 0xfa   
		MOVLB 0   
		MOVLW 0x3   
		MOVWF ctrl_trf_session_owner, BANKED  
		MOVLB 0   
		MOVLW 0xaa   
		MOVWF pDst, BANKED   
		MOVLW 0   
		MOVWF 0xcc, BANKED   
		BRA 0xfa   
		MOVLB 0   
		MOVLW 0x3   
		MOVWF ctrl_trf_session_owner, BANKED  
		MOVLB 0   
		MOVLW 0xaa   
		MOVWF pSrc, BANKED   
		MOVLW 0   
		MOVWF 0xca, BANKED   
		MOVLB 0   
		BCF usb_stat, 0x1, BANKED   
		MOVLB 0   
		MOVLW 0x7   
		MOVWF wCount, BANKED   
		BRA 0xfa   
		MOVLB 0   
		MOVLW 0x3   
		MOVWF ctrl_trf_session_owner, BANKED  
		MOVFF 0x422, control_signal_bitmap   
		;   NOP   
		BRA 0xfa   
		BRA 0xfa   
		RETURN 0   
CDCInitEP    MOVLB 0   
		CLRF line_coding, BANKED   
		MOVLW 0xc2   
		MOVWF 0xab, BANKED   
		MOVLW 0x1   
		MOVWF 0xac, BANKED   
		CLRF 0xad, BANKED   
		CLRF 0xae, BANKED   
		CLRF 0xaf, BANKED   
		MOVLW 0x8   
		MOVWF 0xb0, BANKED   
		CLRF cdc_trf_state, BANKED   
		CLRF cdc_rx_len, BANKED   
		MOVLW 0x1a   
		MOVWF UEP2, ACCESS   
		MOVLW 0x1e   
		MOVWF UEP3, ACCESS   
		MOVLB 0x4   
		MOVLW 0   
		MOVWF 0x16, BANKED   
		MOVLW 0x5   
		MOVWF 0x17, BANKED   
		MOVLW 0x40   
		MOVWF 0x14, BANKED   
		MOVLB 0x4   
		MOVWF 0x19, BANKED   
		MOVLW 0x8   
		MOVWF 0x1a, BANKED   
		MOVLW 0x5   
		MOVWF 0x1b, BANKED   
		MOVLW 0x88   
		MOVWF 0x18, BANKED   
		MOVLB 0x4   
		MOVLW 0x48   
		MOVWF 0x1e, BANKED   
		MOVLW 0x5   
		MOVWF 0x1f, BANKED   
		MOVLW 0x40   
		MOVWF 0x1c, BANKED   
		RETURN 0   
getsUSBUSART    MOVFF FSR2L, POSTINC1   
		;   NOP   
		MOVFF FSR1L, FSR2L   
		;   NOP   
		MOVLB 0   
		CLRF cdc_rx_len, BANKED   
		MOVLB 0x4   
		BTFSC 0x18, 0x7, BANKED   
		BRA 0x1be   
		MOVLW 0xfc   
		MOVFF PLUSW2, __tmp_0   
		;   NOP   
		MOVF 0x19, W, BANKED   
		BSF STATUS, 0, ACCESS   
		SUBFWB __tmp_0, W, ACCESS   
		BC 0x172   
		MOVLW 0xfc   
		MOVFF 0x419, PLUSW2   
		;   NOP   
		MOVLB 0   
		CLRF cdc_rx_len, BANKED   
		MOVLW 0xfc   
		MOVF PLUSW2, W, ACCESS   
		SUBWF cdc_rx_len, W, BANKED   
		BC 0x1b0   
		MOVF cdc_rx_len, W, BANKED   
		CLRF FSR0H, ACCESS   
		ADDLW 0x8   
		MOVWF FSR0L, ACCESS   
		MOVLW 0x5   
		ADDWFC FSR0H, F, ACCESS   
		MOVF INDF0, W, ACCESS   
		MOVWF POSTINC1, ACCESS   
		MOVF cdc_rx_len, W, BANKED   
		MOVWF INDF1, ACCESS   
		MOVLW 0xfd   
		MOVFF PLUSW2, FSR0L   
		;   NOP   
		MOVLW 0xfe   
		MOVFF PLUSW2, FSR0H   
		;   NOP   
		MOVF INDF1, W, ACCESS   
		ADDWF FSR0L, F, ACCESS   
		MOVLW 0   
		ADDWFC FSR0H, F, ACCESS   
		MOVF POSTDEC1, F, ACCESS   
		MOVF INDF1, W, ACCESS   
		MOVWF INDF0, ACCESS   
		INCF cdc_rx_len, F, BANKED   
		BRA 0x176   
		MOVLB 0x4   
		MOVLW 0x40   
		MOVWF 0x19, BANKED   
		ANDWF 0x18, F, BANKED   
		BTG 0x18, 0x6, BANKED   
		MOVLW 0x88   
		IORWF 0x18, F, BANKED   
		MOVLB 0   
		MOVF cdc_rx_len, W, BANKED   
		BRA 0x1c4   
		MOVF POSTDEC1, F, ACCESS   
		MOVFF INDF1, FSR2L   
		;   NOP   
		RETURN 0   
putsUSBUSART    MOVFF FSR2L, POSTINC1   
		;   NOP   
		MOVFF FSR1L, FSR2L   
		;   NOP   
		MOVF POSTINC1, F, ACCESS   
		MOVLB 0   
		MOVF cdc_trf_state, W, BANKED   
		BZ 0x1de   
		BRA 0x23c   
		CLRF INDF2, ACCESS   
		INCF INDF2, F, ACCESS   
		MOVF INDF2, W, ACCESS   
		MOVWF __tmp_0, ACCESS   
		CLRF 0x3, ACCESS   
		MOVLW 0xff   
		XORWF __tmp_0, W, ACCESS   
		BNZ 0x1f0   
		MOVF 0x3, W, ACCESS   
		BNZ 0x1f4   
		BRA 0x20a   
		MOVLW 0xfd   
		MOVFF PLUSW2, FSR0L   
		;   NOP   
		INCF PLUSW2, F, ACCESS   
		MOVLW 0xfe   
		MOVFF PLUSW2, FSR0H   
		;   NOP   
		BNC 0x206   
		INCF PLUSW2, F, ACCESS   
		MOVF INDF0, W, ACCESS   
		BNZ 0x1e0   
		MOVF INDF2, W, ACCESS   
		MOVWF __tmp_0, ACCESS   
		CLRF 0x3, ACCESS   
		MOVF FSR2L, W, ACCESS   
		ADDLW 0xfd   
		MOVWF FSR0L, ACCESS   
		MOVLW 0xff   
		ADDWFC FSR2H, W, ACCESS   
		MOVWF FSR0H, ACCESS   
		MOVF __tmp_0, W, ACCESS   
		SUBWF POSTINC0, F, ACCESS   
		MOVF 0x3, W, ACCESS   
		SUBWFB POSTDEC0, F, ACCESS   
		MOVLW 0xfd   
		MOVFF PLUSW2, pCDCSrc   
		;   NOP   
		MOVLW 0xfe   
		MOVFF PLUSW2, 0xa5   
		;   NOP   
		MOVFF INDF2, cdc_tx_len   
		;   NOP   
		MOVLB 0   
		CLRF cdc_mem_type, BANKED   
		MOVLW 0x1   
		MOVWF cdc_trf_state, BANKED   
		MOVF POSTDEC1, F, ACCESS   
		MOVF POSTDEC1, F, ACCESS   
		MOVFF INDF1, FSR2L   
		;   NOP   
		RETURN 0   
putrsUSBUSART    MOVFF FSR2L, POSTINC1   
		;   NOP   
		MOVFF FSR1L, FSR2L   
		;   NOP   
		MOVF POSTINC1, F, ACCESS   
		MOVLB 0   
		MOVF cdc_trf_state, W, BANKED   
		BZ 0x258   
		BRA 0x2b8   
		CLRF INDF2, ACCESS   
		INCF INDF2, F, ACCESS   
		MOVF INDF2, W, ACCESS   
		MOVWF __tmp_0, ACCESS   
		CLRF 0x3, ACCESS   
		MOVLW 0xff   
		XORWF __tmp_0, W, ACCESS   
		BNZ 0x26a   
		MOVF 0x3, W, ACCESS   
		BNZ 0x26e   
		BRA 0x286   
		MOVLW 0xfd   
		MOVFF PLUSW2, TBLPTR   
		;   NOP   
		INCF PLUSW2, F, ACCESS   
		MOVLW 0xfe   
		MOVFF PLUSW2, TBLPTRH   
		;   NOP   
		BNC 0x280   
		INCF PLUSW2, F, ACCESS   
		TBLRD*  
		MOVF TABLAT, W, ACCESS   
		BNZ 0x25a   
		MOVF INDF2, W, ACCESS   
		MOVWF __tmp_0, ACCESS   
		CLRF 0x3, ACCESS   
		MOVF FSR2L, W, ACCESS   
		ADDLW 0xfd   
		MOVWF FSR0L, ACCESS   
		MOVLW 0xff   
		ADDWFC FSR2H, W, ACCESS   
		MOVWF FSR0H, ACCESS   
		MOVF __tmp_0, W, ACCESS   
		SUBWF POSTINC0, F, ACCESS   
		MOVF 0x3, W, ACCESS   
		SUBWFB POSTDEC0, F, ACCESS   
		MOVLW 0xfd   
		MOVFF PLUSW2, pCDCSrc   
		;   NOP   
		MOVLW 0xfe   
		MOVFF PLUSW2, 0xa5   
		;   NOP   
		MOVFF INDF2, cdc_tx_len   
		;   NOP   
		MOVLB 0   
		MOVLW 0x1   
		MOVWF cdc_mem_type, BANKED   
		MOVWF cdc_trf_state, BANKED   
		MOVF POSTDEC1, F, ACCESS   
		MOVF POSTDEC1, F, ACCESS   
		MOVFF INDF1, FSR2L   
		;   NOP   
		RETURN 0   
CDCTxService    MOVFF FSR2L, POSTINC1   
		;   NOP   
		MOVFF FSR1L, FSR2L   
		;   NOP   
		MOVF POSTINC1, F, ACCESS   
		MOVLB 0x4   
		BTFSC 0x1c, 0x7, BANKED   
		BRA 0x3a8   
		MOVLW 0x3   
		MOVLB 0   
		SUBWF cdc_trf_state, W, BANKED   
		BNZ 0x2dc   
		CLRF cdc_trf_state, BANKED   
		MOVF cdc_trf_state, W, BANKED   
		BNZ 0x2e2   
		BRA 0x3a8   
		MOVLW 0x2   
		SUBWF cdc_trf_state, W, BANKED   
		BNZ 0x2f4   
		MOVLB 0x4   
		CLRF 0x1d, BANKED   
		MOVLB 0   
		MOVLW 0x3   
		MOVWF cdc_trf_state, BANKED   
		BRA 0x39c   
		DECF cdc_trf_state, W, BANKED   
		BNZ 0x39c   
		MOVLW 0x40   
		MOVWF __tmp_0, ACCESS   
		CLRF 0x3, ACCESS   
		MOVF cdc_tx_len, W, BANKED   
		SUBWF __tmp_0, W, ACCESS   
		MOVLW 0   
		SUBWFB 0x3, W, ACCESS   
		BC 0x30e   
		MOVLW 0x40   
		MOVWF INDF2, ACCESS   
		BRA 0x312   
		MOVFF cdc_tx_len, INDF2   
		;   NOP   
		MOVFF INDF2, 0x41d   
		;   NOP   
		MOVF INDF2, W, ACCESS   
		SUBWF cdc_tx_len, W, BANKED   
		MOVWF cdc_tx_len, BANKED   
		MOVLW 0x48   
		MOVWF pCDCDst, BANKED   
		MOVLW 0x5   
		MOVWF 0xa7, BANKED   
		DECF cdc_mem_type, W, BANKED   
		BNZ 0x352   
		MOVF INDF2, W, ACCESS   
		BZ 0x350   
		MOVFF pCDCSrc, TBLPTR   
		;   NOP   
		MOVFF 0xa5, TBLPTRH   
		;   NOP   
		TBLRD*  
		MOVF TABLAT, W, ACCESS   
		MOVFF pCDCDst, FSR0L   
		;   NOP   
		MOVFF 0xa7, FSR0H   
		;   NOP   
		MOVWF INDF0, ACCESS   
		INCF pCDCDst, F, BANKED   
		MOVLW 0   
		ADDWFC 0xa7, F, BANKED   
		INCF pCDCSrc, F, BANKED   
		ADDWFC 0xa5, F, BANKED   
		DECF INDF2, F, ACCESS   
		BRA 0x328   
		BRA 0x378   
		MOVF INDF2, W, ACCESS   
		BZ 0x378   
		MOVFF pCDCSrc, FSR0L   
		;   NOP   
		MOVFF 0xa5, FSR0H   
		;   NOP   
		MOVF INDF0, W, ACCESS   
		MOVFF pCDCDst, FSR0L   
		;   NOP   
		MOVFF 0xa7, FSR0H   
		;   NOP   
		MOVWF INDF0, ACCESS   
		INCF pCDCDst, F, BANKED   
		MOVLW 0   
		ADDWFC 0xa7, F, BANKED   
		INCF pCDCSrc, F, BANKED   
		ADDWFC 0xa5, F, BANKED   
		DECF INDF2, F, ACCESS   
		BRA 0x352   
		MOVF cdc_tx_len, W, BANKED   
		BNZ 0x39c   
		MOVLB 0x4   
		MOVF 0x1d, W, BANKED   
		MOVWF __tmp_0, ACCESS   
		CLRF 0x3, ACCESS   
		MOVLW 0x40   
		XORWF __tmp_0, W, ACCESS   
		BNZ 0x38c   
		MOVF 0x3, W, ACCESS   
		BNZ 0x396   
		MOVLB 0   
		MOVLW 0x2   
		MOVWF cdc_trf_state, BANKED   
		BRA 0x39c   
		MOVLB 0   
		MOVLW 0x3   
		MOVWF cdc_trf_state, BANKED   
		MOVLW 0x40   
		MOVLB 0x4   
		ANDWF 0x1c, F, BANKED   
		BTG 0x1c, 0x6, BANKED   
		MOVLW 0x88   
		IORWF 0x1c, F, BANKED   
		MOVF POSTDEC1, F, ACCESS   
		MOVF POSTDEC1, F, ACCESS   
		MOVFF INDF1, FSR2L   
		;   NOP   
		RETURN 0   
USBCheckStdRequest MOVLB 0x4   
		MOVF 0x20, W, BANKED   
		ANDLW 0x60   
		RRNCF WREG, F, ACCESS   
		RRNCF WREG, F, ACCESS   
		RRNCF WREG, F, ACCESS   
		RRNCF WREG, F, ACCESS   
		RRNCF WREG, F, ACCESS   
		IORLW 0   
		BZ 0x3c8   
		BRA 0x472   
		MOVF 0x21, W, BANKED   
		XORLW 0x7   
		BZ 0x470   
		XORLW 0xb   
		BZ 0x470   
		XORLW 0x7   
		BZ 0x456   
		XORLW 0x1   
		BZ 0x432   
		XORLW 0xb   
		BZ 0x42e   
		XORLW 0x2   
		BZ 0x42e   
		XORLW 0x3   
		BZ 0x42a   
		XORLW 0x8   
		BZ 0x40e   
		XORLW 0x1   
		BZ 0x40a   
		XORLW 0xf   
		BZ 0x406   
		XORLW 0x3   
		BZ 0x3f8   
		BRA 0x470   
		MOVLB 0   
		MOVLW 0x1   
		MOVWF ctrl_trf_session_owner, BANKED  
		MOVLB 0   
		MOVLW 0x4   
		MOVWF usb_device_state, BANKED   
		BRA 0x472   
		RCALL USBStdGetDscHandler   
		BRA 0x472   
		RCALL USBStdSetCfgHandler   
		BRA 0x472   
		MOVLB 0   
		MOVLW 0x1   
		MOVWF ctrl_trf_session_owner, BANKED  
		MOVLB 0   
		MOVLW 0xd1   
		MOVWF pSrc, BANKED   
		MOVLW 0   
		MOVWF 0xca, BANKED   
		MOVLB 0   
		BCF usb_stat, 0x1, BANKED   
		MOVLB 0   
		MOVLW 0x1   
		MOVWF wCount, BANKED   
		BRA 0x472   
		RCALL USBStdGetStatusHandler   
		BRA 0x472   
		RCALL USBStdFeatureReqHandler   
		BRA 0x472   
		MOVLB 0   
		MOVLW 0x1   
		MOVWF ctrl_trf_session_owner, BANKED  
		MOVLB 0x4   
		MOVF 0x24, W, BANKED   
		MOVLB 0   
		MOVWF pSrc, BANKED   
		CLRF 0xca, BANKED   
		MOVLW 0xd2   
		ADDWF pSrc, F, BANKED   
		MOVLW 0   
		ADDWFC 0xca, F, BANKED   
		MOVLB 0   
		BCF usb_stat, 0x1, BANKED   
		MOVLB 0   
		MOVLW 0x1   
		MOVWF wCount, BANKED   
		BRA 0x472   
		MOVLB 0   
		MOVLW 0x1   
		MOVWF ctrl_trf_session_owner, BANKED  
		MOVLB 0x4   
		MOVF 0x24, W, BANKED   
		CLRF FSR0H, ACCESS   
		ADDLW 0xd2   
		MOVWF FSR0L, ACCESS   
		MOVLW 0   
		ADDWFC FSR0H, F, ACCESS   
		MOVFF 0x422, INDF0   
		;   NOP   
		BRA 0x472   
		BRA 0x472   
		RETURN 0   
USBStdGetDscHandler MOVLW 0x80   
		MOVLB 0x4   
		SUBWF 0x20, W, BANKED   
		BNZ 0x52a   
		MOVF 0x23, W, BANKED   
		XORLW 0x3   
		BZ 0x4ea   
		XORLW 0x1   
		BZ 0x4a6   
		XORLW 0x3   
		BZ 0x48c   
		BRA 0x526   
		MOVLB 0   
		MOVLW 0x1   
		MOVWF ctrl_trf_session_owner, BANKED  
		MOVLB 0   
		MOVLW 0x10   
		MOVWF pSrc, BANKED   
		MOVLW 0xc   
		MOVWF 0xca, BANKED   
		MOVLW 0x12   
		MOVLB 0   
		MOVWF wCount, BANKED   
		CLRF 0xce, BANKED   
		BRA 0x526   
		MOVLB 0   
		MOVLW 0x1   
		MOVWF ctrl_trf_session_owner, BANKED  
		MOVLB 0x4   
		MOVF 0x22, W, BANKED   
		MOVWF TBLPTR, ACCESS   
		CLRF TBLPTRH, ACCESS   
		BCF STATUS, 0, ACCESS   
		RLCF TBLPTR, F, ACCESS   
		RLCF TBLPTRH, F, ACCESS   
		MOVLW 0xd1   
		ADDWF TBLPTR, F, ACCESS   
		MOVLW 0xc   
		ADDWFC TBLPTRH, F, ACCESS   
		TBLRD*+  
		MOVFF TABLAT, pSrc   
		;   NOP   
		TBLRD*-  
		MOVFF TABLAT, 0xca   
		;   NOP   
		MOVLW 0x2   
		MOVLB 0   
		CLRF TBLPTRH, ACCESS   
		ADDWF pSrc, W, BANKED   
		MOVWF TBLPTR, ACCESS   
		MOVF 0xca, W, BANKED   
		ADDWFC TBLPTRH, F, ACCESS   
		TBLRD*+  
		MOVFF TABLAT, wCount   
		;   NOP   
		TBLRD*-  
		MOVFF TABLAT, 0xce   
		;   NOP   
		BRA 0x526   
		MOVLB 0   
		MOVLW 0x1   
		MOVWF ctrl_trf_session_owner, BANKED  
		MOVLB 0x4   
		MOVF 0x22, W, BANKED   
		MOVWF TBLPTR, ACCESS   
		CLRF TBLPTRH, ACCESS   
		BCF STATUS, 0, ACCESS   
		RLCF TBLPTR, F, ACCESS   
		RLCF TBLPTRH, F, ACCESS   
		MOVLW 0xd5   
		ADDWF TBLPTR, F, ACCESS   
		MOVLW 0xc   
		ADDWFC TBLPTRH, F, ACCESS   
		TBLRD*+  
		MOVFF TABLAT, pSrc   
		;   NOP   
		TBLRD*-  
		MOVFF TABLAT, 0xca   
		;   NOP   
		MOVFF pSrc, TBLPTR   
		;   NOP   
		MOVFF 0xca, TBLPTRH   
		;   NOP   
		TBLRD*  
		MOVF TABLAT, W, ACCESS   
		MOVLB 0   
		MOVWF wCount, BANKED   
		CLRF 0xce, BANKED   
		BRA 0x526   
		MOVLB 0   
		BSF usb_stat, 0x1, BANKED   
		RETURN 0   
USBStdSetCfgHandler MOVLB 0   
		MOVLW 0x1   
		MOVWF ctrl_trf_session_owner, BANKED  
		MOVLW 0xf   
		MOVWF POSTINC1, ACCESS   
		MOVLW 0x71   
		MOVWF POSTINC1, ACCESS   
		MOVLW 0xf   
		MOVWF POSTINC1, ACCESS   
		CALL ClearArray, 0   
		;   NOP   
		MOVF POSTDEC1, F, ACCESS   
		MOVF POSTDEC1, F, ACCESS   
		MOVF POSTDEC1, F, ACCESS   
		MOVLW 0x1   
		MOVWF POSTINC1, ACCESS   
		MOVLW 0xd2   
		MOVWF POSTINC1, ACCESS   
		MOVLW 0   
		MOVWF POSTINC1, ACCESS   
		CALL ClearArray, 0   
		;   NOP   
		MOVF POSTDEC1, F, ACCESS   
		MOVF POSTDEC1, F, ACCESS   
		MOVF POSTDEC1, F, ACCESS   
		MOVFF 0x422, usb_active_cfg   
		;   NOP   
		MOVLB 0x4   
		MOVF 0x22, W, BANKED   
		BNZ 0x570   
		MOVLB 0   
		MOVLW 0x5   
		MOVWF usb_device_state, BANKED   
		BRA 0x57a   
		MOVLB 0   
		MOVLW 0x6   
		MOVWF usb_device_state, BANKED   
		CALL CDCInitEP, 0   
		;   NOP   
		RETURN 0   
USBStdGetStatusHandler MOVLB 0x4   
		CLRF 0x28, BANKED   
		CLRF 0x29, BANKED   
		MOVLB 0x4   
		MOVF 0x20, W, BANKED   
		ANDLW 0x1f   
		XORLW 0x2   
		BZ 0x5b8   
		XORLW 0x3   
		BZ 0x5b0   
		XORLW 0x1   
		BZ 0x596   
		BRA 0x600   
		MOVLB 0   
		MOVLW 0x1   
		MOVWF ctrl_trf_session_owner, BANKED  
		BTFSS PORTA, 0x2, ACCESS   
		BRA 0x5a4   
		MOVLB 0x4   
		BSF 0x28, 0, BANKED   
		MOVLB 0   
		BTFSS usb_stat, 0, BANKED   
		BRA 0x5ae   
		MOVLB 0x4   
		BSF 0x28, 0x1, BANKED   
		BRA 0x600   
		MOVLB 0   
		MOVLW 0x1   
		MOVWF ctrl_trf_session_owner, BANKED  
		BRA 0x600   
		MOVLB 0   
		MOVLW 0x1   
		MOVWF ctrl_trf_session_owner, BANKED  
		MOVLB 0x4   
		MOVF 0x24, W, BANKED   
		ANDLW 0xf   
		MULLW 0x8   
		MOVF PRODL, W, ACCESS   
		CLRF 0x3, ACCESS   
		ADDLW 0   
		MOVWF __tmp_0, ACCESS   
		MOVLW 0x4   
		ADDWFC 0x3, F, ACCESS   
		MOVF 0x24, W, BANKED   
		ANDLW 0x80   
		BZ 0x5da   
		MOVLW 0x1   
		MULLW 0x4   
		MOVF PRODL, W, ACCESS   
		MOVLB 0   
		ADDWF __tmp_0, W, ACCESS   
		MOVWF pDst, BANKED   
		MOVLW 0   
		ADDWFC 0x3, W, ACCESS   
		MOVWF 0xcc, BANKED   
		MOVFF pDst, FSR0L   
		;   NOP   
		MOVFF 0xcc, FSR0H   
		;   NOP   
		MOVF INDF0, W, ACCESS   
		ANDLW 0x4   
		BZ 0x5fe   
		MOVLB 0x4   
		MOVLW 0x1   
		MOVWF 0x28, BANKED   
		BRA 0x600   
		MOVLB 0   
		DECF ctrl_trf_session_owner, W, BANKED 
		BNZ 0x61a   
		MOVLB 0   
		MOVLW 0x28   
		MOVWF pSrc, BANKED   
		MOVLW 0x4   
		MOVWF 0xca, BANKED   
		MOVLB 0   
		BCF usb_stat, 0x1, BANKED   
		MOVLB 0   
		MOVLW 0x2   
		MOVWF wCount, BANKED   
		RETURN 0   
USBStdFeatureReqHandler MOVLB 0x4   
		DECF 0x22, W, BANKED   
		BNZ 0x642   
		MOVF 0x20, W, BANKED   
		ANDLW 0x1f   
		IORLW 0   
		BNZ 0x642   
		MOVLB 0   
		MOVLW 0x1   
		MOVWF ctrl_trf_session_owner, BANKED  
		MOVLW 0x3   
		MOVLB 0x4   
		SUBWF 0x21, W, BANKED   
		BNZ 0x63e   
		MOVLB 0   
		BSF usb_stat, 0, BANKED   
		BRA 0x642   
		MOVLB 0   
		BCF usb_stat, 0, BANKED   
		MOVLB 0x4   
		MOVF 0x22, W, BANKED   
		BNZ 0x6bc   
		MOVF 0x20, W, BANKED   
		ANDLW 0x1f   
		SUBLW 0x2   
		BNZ 0x6bc   
		MOVF 0x24, W, BANKED   
		ANDLW 0xf   
		IORLW 0   
		BZ 0x6bc   
		MOVLB 0   
		MOVLW 0x1   
		MOVWF ctrl_trf_session_owner, BANKED  
		MOVLB 0x4   
		MOVF 0x24, W, BANKED   
		ANDLW 0xf   
		MULLW 0x8   
		MOVF PRODL, W, ACCESS   
		CLRF 0x3, ACCESS   
		ADDLW 0   
		MOVWF __tmp_0, ACCESS   
		MOVLW 0x4   
		ADDWFC 0x3, F, ACCESS   
		MOVF 0x24, W, BANKED   
		ANDLW 0x80   
		BZ 0x67a   
		MOVLW 0x1   
		MULLW 0x4   
		MOVF PRODL, W, ACCESS   
		MOVLB 0   
		ADDWF __tmp_0, W, ACCESS   
		MOVWF pDst, BANKED   
		MOVLW 0   
		ADDWFC 0x3, W, ACCESS   
		MOVWF 0xcc, BANKED   
		MOVLW 0x3   
		MOVLB 0x4   
		SUBWF 0x21, W, BANKED   
		BNZ 0x6a0   
		MOVLW 0x84   
		MOVFF pDst, FSR0L   
		;   NOP   
		MOVFF 0xcc, FSR0H   
		;   NOP   
		MOVWF INDF0, ACCESS   
		BRA 0x6bc   
		BTFSS 0x24, 0x7, BANKED   
		BRA 0x6b0   
		MOVFF pDst, FSR0L   
		;   NOP   
		MOVFF 0xcc, FSR0H   
		;   NOP   
		CLRF INDF0, ACCESS   
		BRA 0x6bc   
		MOVLW 0x88   
		MOVFF pDst, FSR0L   
		;   NOP   
		MOVFF 0xcc, FSR0H   
		;   NOP   
		MOVWF INDF0, ACCESS   
		RETURN 0   
USBCtrlEPService  MOVF USTAT, W, ACCESS   
		BZ 0x6c6   
		MOVLW 0   
		BRA 0x6c8   
		MOVLW 0x1   
		IORLW 0   
		BZ 0x6e2   
		MOVLB 0x4   
		MOVF buffer, W, BANKED   
		ANDLW 0x3c   
		RRNCF WREG, F, ACCESS   
		RRNCF WREG, F, ACCESS   
		SUBLW 0xd   
		BNZ 0x6de   
		RCALL USBCtrlTrfSetupHandler   
		BRA 0x6e0   
		RCALL USBCtrlTrfOutHandler   
		BRA 0x6f2   
		MOVF USTAT, W, ACCESS   
		BZ 0x6ea   
		MOVLW 0   
		BRA 0x6ec   
		MOVLW 0x1   
		IORLW 0x4   
		BZ 0x6f2   
		RCALL USBCtrlTrfInHandler   
		RETURN 0   
USBCtrlTrfSetupHandler MOVFF FSR2L, POSTINC1   
		;   NOP   
		MOVFF FSR1L, FSR2L   
		;   NOP   
		MOVF POSTINC1, F, ACCESS   
		MOVLB 0   
		CLRF ctrl_trf_state, BANKED   
		CLRF ctrl_trf_session_owner, BANKED   
		CLRF wCount, BANKED   
		CLRF 0xce, BANKED   
		CALL USBCheckStdRequest, 0   
		;   NOP   
		CLRF INDF2, ACCESS   
		MOVF INDF2, W, ACCESS   
		MOVWF __tmp_0, ACCESS   
		CLRF 0x3, ACCESS   
		MOVLW 0x1   
		SUBWF __tmp_0, W, ACCESS   
		MOVLW 0   
		SUBWFB 0x3, W, ACCESS   
		BC 0x754   
		MOVLB 0   
		MOVF ctrl_trf_session_owner, W, BANKED 
		BZ 0x726   
		BRA 0x754   
		CLRF TBLPTRH, ACCESS   
		RLCF INDF2, W, ACCESS   
		ANDLW 0xfe   
		RLCF TBLPTRH, F, ACCESS   
		MOVWF TBLPTR, ACCESS   
		MOVLW 0xdb   
		ADDWF TBLPTR, F, ACCESS   
		MOVLW 0xc   
		ADDWFC TBLPTRH, F, ACCESS   
		TBLRD*+  
		MOVFF TABLAT, __tmp_0   
		;   NOP   
		TBLRD*  
		MOVFF TABLAT, 0x3   
		;   NOP   
		BRA 0x74e   
		MOVFF 0x3, PCLATH   
		;   NOP   
		MOVF __tmp_0, W, ACCESS   
		MOVWF PCL, ACCESS   
		RCALL 0x746   
		INCF INDF2, F, ACCESS   
		BRA 0x70e   
		RCALL USBCtrlEPServiceComplete   
		MOVF POSTDEC1, F, ACCESS   
		MOVF POSTDEC1, F, ACCESS   
		MOVFF INDF1, FSR2L   
		;   NOP   
		RETURN 0   
USBCtrlTrfOutHandler MOVLW 0x2   
		MOVLB 0   
		SUBWF ctrl_trf_state, W, BANKED   
		BNZ 0x77c   
		RCALL USBCtrlTrfRxService   
		MOVLB 0x4   
		BTFSC buffer, 0x6, BANKED   
		BRA 0x776   
		MOVLW 0xc8   
		MOVWF buffer, BANKED   
		BRA 0x77a   
		MOVLW 0x88   
		MOVWF buffer, BANKED   
		BRA 0x77e   
		RCALL USBPrepareForNextSetupTrf   
		RETURN 0   
USBCtrlTrfInHandler MOVLW 0x4   
		MOVLB 0   
		SUBWF usb_device_state, W, BANKED   
		BNZ 0x79c   
		MOVFF 0x422, UADDR   
		;   NOP   
		MOVF UADDR, W, ACCESS   
		SUBLW 0   
		BC 0x798   
		MOVLW 0x5   
		MOVWF usb_device_state, BANKED   
		BRA 0x79c   
		MOVLW 0x3   
		MOVWF usb_device_state, BANKED   
		MOVLB 0   
		DECF ctrl_trf_state, W, BANKED   
		BNZ 0x7b6   
		RCALL USBCtrlTrfTxService   
		MOVLB 0x4   
		BTFSC 0x4, 0x6, BANKED   
		BRA 0x7b0   
		MOVLW 0xc8   
		MOVWF 0x4, BANKED   
		BRA 0x7b4   
		MOVLW 0x88   
		MOVWF 0x4, BANKED   
		BRA 0x7b8   
		RCALL USBPrepareForNextSetupTrf   
		RETURN 0   
USBCtrlTrfTxService MOVFF FSR2L, POSTINC1   
		;   NOP   
		MOVFF FSR1L, FSR2L   
		;   NOP   
		MOVLW 0x2   
		ADDWF FSR1L, F, ACCESS   
		MOVLB 0   
		MOVLW 0x8   
		SUBWF wCount, W, BANKED   
		MOVLW 0   
		SUBWFB 0xce, W, BANKED   
		BC 0x7dc   
		MOVFF wCount, POSTINC2   
		;   NOP   
		MOVFF 0xce, POSTDEC2   
		;   NOP   
		BRA 0x7e2   
		MOVLW 0x8   
		MOVWF POSTINC2, ACCESS   
		CLRF POSTDEC2, ACCESS   
		MOVLB 0x4   
		BCF 0x4, 0x1, BANKED   
		BCF 0x4, 0, BANKED   
		MOVLW 0x1   
		MOVF PLUSW2, W, ACCESS   
		IORWF 0x4, F, BANKED   
		MOVFF INDF2, 0x405   
		;   NOP   
		MOVLW 0   
		MOVF PLUSW2, W, ACCESS   
		MOVLB 0   
		SUBWF wCount, F, BANKED   
		MOVLW 0x1   
		MOVF PLUSW2, W, ACCESS   
		SUBWFB 0xce, F, BANKED   
		MOVLW 0x28   
		MOVWF pDst, BANKED   
		MOVLW 0x4   
		MOVWF 0xcc, BANKED   
		MOVLB 0   
		BTFSS usb_stat, 0x1, BANKED   
		BRA 0x84a   
		MOVFF FSR2L, FSR0L   
		;   NOP   
		MOVFF FSR2H, FSR0H   
		;   NOP   
		MOVF POSTINC0, W, ACCESS   
		IORWF POSTDEC0, W, ACCESS   
		BZ 0x848   
		MOVFF pSrc, TBLPTR   
		;   NOP   
		MOVFF 0xca, TBLPTRH   
		;   NOP   
		TBLRD*  
		MOVF TABLAT, W, ACCESS   
		MOVFF pDst, FSR0L   
		;   NOP   
		MOVFF 0xcc, FSR0H   
		;   NOP   
		MOVWF INDF0, ACCESS   
		MOVLB 0   
		INCF pDst, F, BANKED   
		MOVLW 0   
		ADDWFC 0xcc, F, BANKED   
		INCF pSrc, F, BANKED   
		ADDWFC 0xca, F, BANKED   
		DECF INDF2, F, ACCESS   
		MOVLW 0x1   
		BC 0x846   
		DECF PLUSW2, F, ACCESS   
		BRA 0x80e   
		BRA 0x882   
		MOVFF FSR2L, FSR0L   
		;   NOP   
		MOVFF FSR2H, FSR0H   
		;   NOP   
		MOVF POSTINC0, W, ACCESS   
		IORWF POSTDEC0, W, ACCESS   
		BZ 0x882   
		MOVFF pSrc, FSR0L   
		;   NOP   
		MOVFF 0xca, FSR0H   
		;   NOP   
		MOVF INDF0, W, ACCESS   
		MOVFF pDst, FSR0L   
		;   NOP   
		MOVFF 0xcc, FSR0H   
		;   NOP   
		MOVWF INDF0, ACCESS   
		MOVLB 0   
		INCF pDst, F, BANKED   
		MOVLW 0   
		ADDWFC 0xcc, F, BANKED   
		INCF pSrc, F, BANKED   
		ADDWFC 0xca, F, BANKED   
		DECF INDF2, F, ACCESS   
		MOVLW 0x1   
		BC 0x880   
		DECF PLUSW2, F, ACCESS   
		BRA 0x84a   
		MOVFF FSR2L, FSR1L   
		;   NOP   
		MOVF POSTDEC1, F, ACCESS   
		MOVFF INDF1, FSR2L   
		;   NOP   
		RETURN 0   
USBCtrlTrfRxService MOVFF FSR2L, POSTINC1   
		;   NOP   
		MOVFF FSR1L, FSR2L   
		;   NOP   
		MOVLW 0x2   
		ADDWF FSR1L, F, ACCESS   
		MOVLW 0x3   
		MOVLB 0x4   
		ANDWF buffer, W, BANKED   
		MOVWF INDF1, ACCESS   
		MOVLW 0x1   
		MOVFF INDF1, PLUSW2   
		;   NOP   
		MOVFF 0x401, INDF2   
		;   NOP   
		MOVF POSTINC2, W, ACCESS   
		MOVLB 0   
		ADDWF wCount, F, BANKED   
		MOVF POSTDEC2, W, ACCESS   
		ADDWFC 0xce, F, BANKED   
		MOVLW 0x28   
		MOVWF pSrc, BANKED   
		MOVLW 0x4   
		MOVWF 0xca, BANKED   
		MOVFF FSR2L, FSR0L   
		;   NOP   
		MOVFF FSR2H, FSR0H   
		;   NOP   
		MOVF POSTINC0, W, ACCESS   
		IORWF POSTDEC0, W, ACCESS   
		BZ 0x8f4   
		MOVFF pSrc, FSR0L   
		;   NOP   
		MOVFF 0xca, FSR0H   
		;   NOP   
		MOVF INDF0, W, ACCESS   
		MOVFF pDst, FSR0L   
		;   NOP   
		MOVFF 0xcc, FSR0H   
		;   NOP   
		MOVWF INDF0, ACCESS   
		INCF pDst, F, BANKED   
		MOVLW 0   
		ADDWFC 0xcc, F, BANKED   
		INCF pSrc, F, BANKED   
		ADDWFC 0xca, F, BANKED   
		DECF INDF2, F, ACCESS   
		MOVLW 0x1   
		BC 0x8f2   
		DECF PLUSW2, F, ACCESS   
		BRA 0x8be   
		MOVFF FSR2L, FSR1L   
		;   NOP   
		MOVF POSTDEC1, F, ACCESS   
		MOVFF INDF1, FSR2L   
		;   NOP   
		RETURN 0   
USBCtrlEPServiceComplete MOVLB 0   
		MOVF ctrl_trf_session_owner, W, BANKED 
		BNZ 0x91e   
		MOVLB 0x4   
		MOVLW 0x8   
		MOVWF 0x1, BANKED   
		MOVLW 0x20   
		MOVWF __tmp_0, BANKED   
		MOVLW 0x4   
		MOVWF 0x3, BANKED   
		MOVLW 0x84   
		MOVWF buffer, BANKED   
		MOVLB 0x4   
		MOVWF 0x4, BANKED   
		BRA 0x988   
		MOVLB 0x4   
		BTFSS 0x20, 0x7, BANKED   
		BRA 0x968   
		MOVLB 0   
		MOVF wCount, W, BANKED   
		MOVLB 0x4   
		SUBWF 0x26, W, BANKED   
		MOVLB 0   
		MOVF 0xce, W, BANKED   
		MOVLB 0x4   
		SUBWFB 0x27, W, BANKED   
		BC 0x93e   
		MOVFF 0x426, wCount   
		;   NOP   
		MOVFF 0x427, 0xce   
		;   NOP   
		RCALL USBCtrlTrfTxService   
		MOVLB 0   
		MOVLW 0x1   
		MOVWF ctrl_trf_state, BANKED   
		MOVLB 0x4   
		MOVLW 0x8   
		MOVWF 0x1, BANKED   
		MOVLW 0x20   
		MOVWF __tmp_0, BANKED   
		MOVLW 0x4   
		MOVWF 0x3, BANKED   
		MOVLW 0x80   
		MOVWF buffer, BANKED   
		MOVLB 0x4   
		MOVLW 0x28   
		MOVWF 0x6, BANKED   
		MOVLW 0x4   
		MOVWF 0x7, BANKED   
		MOVLW 0xc8   
		MOVWF 0x4, BANKED   
		BRA 0x988   
		MOVLB 0   
		MOVLW 0x2   
		MOVWF ctrl_trf_state, BANKED   
		MOVLB 0x4   
		CLRF 0x5, BANKED   
		MOVLW 0xc8   
		MOVWF 0x4, BANKED   
		MOVLB 0x4   
		MOVLW 0x8   
		MOVWF 0x1, BANKED   
		MOVLW 0x28   
		MOVWF __tmp_0, BANKED   
		MOVLW 0x4   
		MOVWF 0x3, BANKED   
		MOVLW 0xc8   
		MOVWF buffer, BANKED   
		BCF UCON, 0x4, ACCESS   
		RETURN 0   
USBPrepareForNextSetupTrf MOVLB 0   
		CLRF ctrl_trf_state, BANKED   
		MOVLB 0x4   
		MOVLW 0x8   
		MOVWF 0x1, BANKED   
		MOVLW 0x20   
		MOVWF __tmp_0, BANKED   
		MOVLW 0x4   
		MOVWF 0x3, BANKED   
		MOVLW 0x88   
		MOVWF buffer, BANKED   
		MOVLB 0x4   
		CLRF 0x4, BANKED   
		RETURN 0   
USBCheckBusStatus BTFSS PORTA, 0x1, ACCESS   
		BRA 0x9b2   
		BTFSS UCON, 0x3, ACCESS   
		RCALL USBModuleEnable   
		BRA 0x9b6   
		BTFSC UCON, 0x3, ACCESS   
		RCALL USBModuleDisable   
		MOVLB 0   
		DECF usb_device_state, W, BANKED   
		BNZ 0x9cc   
		BTFSC UCON, 0x5, ACCESS   
		BRA 0x9cc   
		CLRF UIR, ACCESS   
		CLRF UIE, ACCESS   
		BSF UIE, 0, ACCESS   
		BSF UIE, 0x4, ACCESS   
		MOVLW 0x2   
		MOVWF usb_device_state, BANKED   
		RETURN 0   
USBModuleEnable  CLRF UCON, ACCESS   
		CLRF UIE, ACCESS   
		BSF UCON, 0x3, ACCESS   
		MOVLB 0   
		MOVLW 0x1   
		MOVWF usb_device_state, BANKED   
		RETURN 0   
USBModuleDisable  CLRF UCON, ACCESS   
		CLRF UIE, ACCESS   
		MOVLB 0   
		CLRF usb_device_state, BANKED   
		RETURN 0   
USBSoftDetach    GOTO USBModuleDisable   
;   NOP   
USBDriverService  MOVLB 0   
		MOVF usb_device_state, W, BANKED   
		BNZ 0x9f2   
		BRA 0xa66   
		MOVF UIR, W, ACCESS   
		ANDLW 0x4   
		BZ 0xa00   
		MOVF UIE, W, ACCESS   
		ANDLW 0x4   
		BZ 0xa00   
		RCALL USBWakeFromSuspend   
		BTFSC UCON, 0x1, ACCESS   
		BRA 0xa66   
		MOVF UIR, W, ACCESS   
		ANDLW 0x1   
		BZ 0xa12   
		MOVF UIE, W, ACCESS   
		ANDLW 0x1   
		BZ 0xa12   
		RCALL USBProtocolResetHandler   
		MOVF UIR, W, ACCESS   
		ANDLW 0x10   
		BZ 0xa20   
		MOVF UIE, W, ACCESS   
		ANDLW 0x10   
		BZ 0xa20   
		RCALL USBSuspend   
		MOVF UIR, W, ACCESS   
		ANDLW 0x40   
		BZ 0xa2e   
		MOVF UIE, W, ACCESS   
		ANDLW 0x40   
		BZ 0xa2e   
		RCALL USB_SOF_Handler   
		MOVF UIR, W, ACCESS   
		ANDLW 0x20   
		BZ 0xa3c   
		MOVF UIE, W, ACCESS   
		ANDLW 0x20   
		BZ 0xa3c   
		RCALL USBStallHandler   
		MOVF UIR, W, ACCESS   
		ANDLW 0x2   
		BZ 0xa4a   
		MOVF UIE, W, ACCESS   
		ANDLW 0x2   
		BZ 0xa4a   
		RCALL USBErrorHandler   
		MOVLW 0x3   
		MOVLB 0   
		SUBWF usb_device_state, W, BANKED   
		BC 0xa54   
		BRA 0xa66   
		MOVF UIR, W, ACCESS   
		ANDLW 0x8   
		BZ 0xa66   
		MOVF UIE, W, ACCESS   
		ANDLW 0x8   
		BZ 0xa66   
		CALL USBCtrlEPService, 0   
		;   NOP   
		BCF UIR, 0x3, ACCESS   
		RETURN 0   
USBSuspend    BSF UIE, 0x2, ACCESS   
		BCF UIR, 0x4, ACCESS   
		BSF UCON, 0x1, ACCESS   
		BCF PIR2, 0x5, ACCESS   
		BSF PIE2, 0x5, ACCESS   
		SLEEP   
		BCF PIE2, 0x5, ACCESS   
___return_lbl00028 RETURN 0   
USBWakeFromSuspend BCF UCON, 0x1, ACCESS   
		BCF UIE, 0x2, ACCESS   
		BCF UIR, 0x2, ACCESS   
		RETURN 0   
USBRemoteWakeup  MOVLB 0   
		BTFSS usb_stat, 0, BANKED   
		BRA 0xaa2   
		RCALL USBWakeFromSuspend   
		BSF UCON, 0x2, ACCESS   
		MOVLB 0   
		MOVLW 0x8   
		MOVWF delay_count, BANKED   
		MOVLW 0x7   
		MOVWF 0xd4, BANKED   
		DECF delay_count, F, BANKED   
		MOVLW 0   
		SUBWFB 0xd4, F, BANKED   
		MOVF delay_count, W, BANKED   
		IORWF 0xd4, W, BANKED   
		BNZ 0xa94   
		BCF UCON, 0x2, ACCESS   
		RETURN 0   
USB_SOF_Handler  BCF UIR, 0x6, ACCESS   
		RETURN 0   
USBStallHandler  BTFSS UEP0, 0, ACCESS   
		BRA 0xab2   
		CALL USBPrepareForNextSetupTrf, 0   
		;   NOP   
		BCF UEP0, 0, ACCESS   
		BCF UIR, 0x5, ACCESS   
		RETURN 0   
USBErrorHandler  BCF UIR, 0x1, ACCESS   
		RETURN 0   
USBProtocolResetHandler CLRF UEIR, ACCESS   
		CLRF UIR, ACCESS   
		MOVLW 0x9f   
		MOVWF UEIE, ACCESS   
		MOVLW 0x7b   
		MOVWF UIE, ACCESS   
		CLRF UADDR, ACCESS   
		MOVLW 0xf   
		MOVWF POSTINC1, ACCESS   
		MOVLW 0x71   
		MOVWF POSTINC1, ACCESS   
		MOVLW 0xf   
		MOVWF POSTINC1, ACCESS   
		RCALL ClearArray   
		MOVF POSTDEC1, F, ACCESS   
		MOVF POSTDEC1, F, ACCESS   
		MOVF POSTDEC1, F, ACCESS   
		MOVLW 0x16   
		MOVWF UEP0, ACCESS   
		BTFSS UIR, 0x3, ACCESS   
		BRA 0xae8   
		BCF UIR, 0x3, ACCESS   
		BRA 0xae0   
		BCF UCON, 0x4, ACCESS   
		CALL USBPrepareForNextSetupTrf, 0   
		;   NOP   
		MOVLB 0   
		BCF usb_stat, 0, BANKED   
		MOVLB 0   
		CLRF usb_active_cfg, BANKED   
		MOVLB 0   
		MOVLW 0x3   
		MOVWF usb_device_state, BANKED   
		RETURN 0   
ClearArray    MOVFF FSR2L, POSTINC1   
		;   NOP   
		MOVFF FSR1L, FSR2L   
		;   NOP   
		MOVLW 0xfd   
		MOVFF PLUSW2, FSR0L   
		;   NOP   
		MOVLW 0xfe   
		MOVFF PLUSW2, FSR0H   
		;   NOP   
___while_lbl00043 MOVLW 0xfc   
		MOVF PLUSW2, W, ACCESS   
		BZ ___while_lbl00044   
		CLRF POSTINC0, ACCESS   
		MOVLW 0xfc   
		DECF PLUSW2, F, ACCESS   
		BRA ___while_lbl00043   
___while_lbl00044 MOVF POSTDEC1, F, ACCESS   
		MOVFF INDF1, FSR2L   
		;   NOP   
		RETURN 0   
UserInit    BRA UserInit2	;MOVLW 0xf8   
		ANDWF LATB, F, ACCESS   
		ANDWF TRISB, F, ACCESS   
		RETURN 0   
ProcessIO    RCALL BlinkUSBStatus   
		MOVLW 0x6   
		MOVLB 0   
		SUBWF usb_device_state, W, BANKED   
		BNC 0xb40   
		MOVF UCON, W, ACCESS   
		ANDLW 0x2   
		BNZ 0xb7e; BZ 0xb42   ; ***
		RCALL CDC_Idle;BRA 0xb7e   
		MOVLW 0x1 ; length   
		MOVWF POSTINC1, ACCESS   
		MOVLW input_buffer   
		MOVWF POSTINC1, ACCESS   
		MOVLW 0   
		MOVWF POSTINC1, ACCESS   
		CALL getsUSBUSART, 0   
		;   NOP   
		MOVF POSTDEC1, F, ACCESS   
		MOVF POSTDEC1, F, ACCESS   
		MOVF POSTDEC1, F, ACCESS   
		IORLW 0   
		BZ 0xb7e   
		BRA CDC_RX_char_received ;MOVLB 0   
		MOVF cdc_trf_state, W, BANKED   ; dead code until BlinkUSBStatus
		BNZ 0xb7e   
		MOVLB 0   
		BRA CDC_RX_char_received; INCF io_buffer, F, BANKED
		MOVLB 0   ; dead code until BlinkUSBStatus   
		MOVLW output_buffer   
		MOVWF pCDCSrc, BANKED   
		MOVLW 0   
		MOVWF pCDCSrcMSB, BANKED   
		MOVLB 0   
		MOVLW 0x1   
		MOVWF cdc_tx_len, BANKED   
		MOVLB 0   
		CLRF cdc_mem_type, BANKED   
		MOVLB 0   
		MOVWF cdc_trf_state, BANKED   
		RETURN 0   
BlinkUSBStatus   MOVLB 0   
		MOVF led_count, W, BANKED   
		IORWF 0xd6, W, BANKED   
		BNZ 0xb90   
		MOVLW 0x10   
		MOVWF led_count, BANKED   
		MOVLW 0x27   
		MOVWF 0xd6, BANKED   
		DECF led_count, F, BANKED   
		MOVLW 0   
		SUBWFB 0xd6, F, BANKED   
		BTFSS UCON, 0x1, ACCESS   
		BRA 0xbb0   
		MOVF led_count, W, BANKED   
		IORWF 0xd6, W, BANKED   
		BNZ 0xbae   
		BTG LATB, 0, ACCESS   
		MOVF LATB, W, ACCESS   
		ANDLW 0x1   
		BZ 0xbac   
		BSF LATB, 0x1, ACCESS   
		BRA 0xbae   
		BCF LATB, 0x1, ACCESS   
		BRA 0xc0e   
		MOVLB 0   
		MOVF usb_device_state, W, BANKED   
		BNZ 0xbbc   
		BCF LATB, 0, ACCESS   
		BCF LATB, 0x1, ACCESS   
		BRA 0xc0e   
		DECF usb_device_state, W, BANKED   
		BNZ 0xbc6   
		BSF LATB, 0, ACCESS   
		BSF LATB, 0x1, ACCESS   
		BRA 0xc0e   
		MOVLW 0x2   
		SUBWF usb_device_state, W, BANKED   
		BNZ 0xbd2   
		BSF LATB, 0, ACCESS   
		BCF LATB, 0x1, ACCESS   
		BRA 0xc0e   
		MOVLW 0x3   
		SUBWF usb_device_state, W, BANKED   
		BNZ 0xbde   
		BCF LATB, 0, ACCESS   
		BSF LATB, 0x1, ACCESS   
		BRA 0xc0e   
		MOVLW 0x5   
		SUBWF usb_device_state, W, BANKED   
		BNZ 0xbf2   
		MOVLB 0   
		MOVF led_count, W, BANKED   
		IORWF 0xd6, W, BANKED   
		BNZ 0xbf0   
		BTG LATB, 0, ACCESS   
		BCF LATB, 0x1, ACCESS   
		BRA 0xc0e   
		MOVLW 0x6   
		SUBWF usb_device_state, W, BANKED   
		BNZ 0xc0e   
		MOVLB 0   
		MOVF led_count, W, BANKED   
		IORWF 0xd6, W, BANKED   
		BNZ 0xc0e   
		BTG LATB, 0, ACCESS   
		MOVF LATB, W, ACCESS   
		ANDLW 0x1   
		BZ 0xc0c   
		BCF LATB, 0x1, ACCESS   
		BRA 0xc0e   
		BSF LATB, 0x1, ACCESS   
		RETURN 0   
		dw 0x0112   
		;   MULWF buffer, ACCESS   
		dw 0x0200
			dw 0x0002   
		SUBLW 0   
		DECF STATUS, W, ACCESS   
		TBLRD*-  
		NOP   ;;;;;;;;;;;;;;  
		MULWF 0x1, ACCESS
			  
		MOVLB 0   
		MULWF 0x9, ACCESS   
		dw 0x0043   
		MOVLB 0x2   
		BSF buffer, 0, ACCESS   
		IORLW 0x32   
		CLRWDT   
		MOVLB 0   
		MULWF __tmp_0, ACCESS   
		dw 0x0001   
		ADDWF 0x5, W, ACCESS   
		IORWF buffer, W, ACCESS   
		DECF 0x1, W, ACCESS   
		MULWF 0x24, ACCESS   
		DECF __tmp_0, W, BANKED   
		DECF 0x24, F, ACCESS   
		MOVLB 0   
		ADDWF 0x5, W, ACCESS   
		dw 0x0001   
		DECF 0x1, F, BANKED   
		BSF 0x5, 0x1, ACCESS   
		SUBLW 0x3   
		MULWF buffer, ACCESS   
		DECF 0x9, W, ACCESS   
		dw 0x0001   
		XORLW 0x2   
		NOP   
		DECF buffer, F, BANKED   
		MULWF 0x5, BANKED   
		RRNCF __tmp_0, W, ACCESS   
		NOP   
		DECF 0x7, W, BANKED   
		MULWF 0xf83, ACCESS   
		dw 0x0040   
		DECF buffer, W, ACCESS   
		IORLW 0x3   
		RLCF 0x4, W, ACCESS   
		DCFSNZ 0x3, W, BANKED   
		SETF buffer, BANKED   
		CPFSEQ buffer, BANKED   
		BTG buffer, 0x1, ACCESS   
		MOVWF buffer, BANKED   
		CPFSEQ buffer, BANKED   
		SETF buffer, ACCESS   
		SETF buffer, BANKED   
		BTG buffer, 0, ACCESS   
		ADDWFC buffer, W, ACCESS   
		SUBFWB buffer, W, ACCESS   
		CPFSGT buffer, BANKED   
		CPFSEQ buffer, BANKED   
		SETF buffer, ACCESS   
		MOVWF buffer, ACCESS   
		MOVWF buffer, BANKED   
		NEGF buffer, ACCESS   
		MOVWF buffer, BANKED   
		TSTFSZ buffer, BANKED   
		BTG buffer, 0x4, BANKED   
		ADDWFC buffer, W, ACCESS   
		INFSNZ buffer, W, BANKED   
		MOVWF buffer, ACCESS   
		CPFSEQ buffer, BANKED   
		DECFSZ buffer, F, ACCESS   
		RLCF buffer, W, ACCESS   
		RRNCF 0x3, F, BANKED   
		RLNCF buffer, W, ACCESS   
		RRNCF buffer, F, BANKED   
		ADDWFC buffer, W, ACCESS   
		MOVF buffer, F, ACCESS   
		MOVF buffer, F, BANKED   
		DECFSZ buffer, W, BANKED   
		RRCF buffer, F, ACCESS   
		RRCF buffer, F, BANKED   
		RRCF buffer, F, ACCESS   
		ADDWFC buffer, W, ACCESS   
		RLNCF buffer, W, BANKED   
		NEGF buffer, BANKED   
		BTG buffer, 0x2, BANKED   
		NEGF buffer, ACCESS   
		CPFSLT buffer, BANKED   
		BTG buffer, 0x2, ACCESS   
		SETF buffer, BANKED   
		MOVWF buffer, BANKED   
		MOVWF buffer, ACCESS   
		ADDWFC buffer, W, ACCESS   
		RLNCF buffer, W, ACCESS   
		CPFSGT buffer, BANKED   
		NEGF buffer, BANKED   
		MOVWF buffer, BANKED   
		ADDWFC buffer, F, ACCESS   
		ADDWFC 0xc, F, ACCESS   
		CPFSGT 0xc, BANKED   
		SETF 0xc, BANKED   
		BCF 0xc, 0x6, BANKED   
		SWAPF 0xc, W, ACCESS   
		dw 0xFF00   
_do_cinit    MOVLW 0x2a   
		MOVWF TBLPTR, ACCESS   
		MOVLW 0   
		MOVWF TBLPTRH, ACCESS   
		MOVLW 0   
		MOVWF TBLPTRU, ACCESS   
		MOVLB 0   
		TBLRD*+  
		MOVF TABLAT, W, ACCESS   
		MOVWF curr_entry, BANKED   
		TBLRD*+  
		MOVF TABLAT, W, ACCESS   
		MOVWF 0xc3, BANKED   
test    BNZ 0xd00   
		TSTFSZ curr_entry, BANKED   
		BRA 0xd00   
		BRA done   
		TBLRD*+  
		MOVF TABLAT, W, ACCESS   
		MOVWF prom, BANKED   
		TBLRD*+  
		MOVF TABLAT, W, ACCESS   
		MOVWF 0xbe, BANKED   
		TBLRD*+  
		MOVF TABLAT, W, ACCESS   
		MOVWF 0xbf, BANKED   
		TBLRD*+  
		TBLRD*+  
		MOVF TABLAT, W, ACCESS   
		MOVWF FSR0L, ACCESS   
		TBLRD*+  
		MOVF TABLAT, W, ACCESS   
		MOVWF FSR0H, ACCESS   
		TBLRD*+  
		TBLRD*+  
		TBLRD*+  
		MOVF TABLAT, W, ACCESS   
		MOVWF curr_byte, BANKED   
		TBLRD*+  
		MOVF TABLAT, W, ACCESS   
		MOVWF 0xc1, BANKED   
		TBLRD*+  
		TBLRD*+  
		MOVFF TBLPTR, data_ptr   
		;   NOP   
		MOVFF TBLPTRH, 0xc5   
		;   NOP   
		MOVFF TBLPTRU, 0xc6   
		;   NOP   
		MOVFF prom, TBLPTR   
		;   NOP   
		MOVFF 0xbe, TBLPTRH   
		;   NOP   
		MOVFF 0xbf, TBLPTRU   
		;   NOP   
		MOVLB 0   
		MOVF curr_byte, F, BANKED   
copy_loop    BNZ copy_one_byte   
		MOVF 0xc1, F, BANKED   
		BZ done_copying   
copy_one_byte    TBLRD*+  
		MOVF TABLAT, W, ACCESS   
		MOVWF POSTINC0, ACCESS   
		DECF curr_byte, F, BANKED   
		BC copy_loop   
		DECF 0xc1, F, BANKED   
		BRA copy_one_byte   
done_copying    MOVFF data_ptr, TBLPTR   
		;   NOP   
		MOVFF 0xc5, TBLPTRH   
		;   NOP   
		MOVFF 0xc6, TBLPTRU   
		;   NOP   
		MOVLB 0   
		DECF curr_entry, F, BANKED   
		MOVLW 0   
		SUBWFB 0xc3, F, BANKED   
		BRA test   
done    RETURN 0   
main    RCALL InitializeSystem   
		RCALL USBTasks   
		CALL ProcessIO, 0   
		;   NOP   
		BRA 0xd7e   
		RETURN 0   
InitializeSystem  MOVLW 0xf   
		IORWF ADCON1, F, ACCESS   
		BSF TRISA, 0x1, ACCESS   
		BSF TRISA, 0x2, ACCESS   
		MOVLW 0x14   
		MOVWF UCFG, ACCESS   
		MOVLB 0   
		CLRF usb_device_state, BANKED   
		MOVLB 0   
		CLRF usb_stat, BANKED   
		MOVLB 0   
		CLRF usb_active_cfg, BANKED   
		GOTO UserInit   
		;   NOP   
USBTasks    CALL USBCheckBusStatus, 0   
		;   NOP   
		BTFSC UCFG, 0x7, ACCESS   
		BRA 0xdb0   
		CALL USBDriverService, 0   
		;   NOP   
		GOTO CDCTxService   
		;   NOP   
_startup    LFSR 0x1, 0x300   
		;   NOP   
		LFSR 0x2, 0x300   
		;   NOP   
		CLRF TBLPTRU, ACCESS   
		BCF 0x1, 0x6, ACCESS   
		CALL _do_cinit, 0   
		;   NOP   
loop    CALL main, 0   
		;   NOP   
		BRA loop   
___return_lbl00001 RETURN 0   

; assembly user code:

UserInit2:
		MOVLW 	0xf8   
		ANDWF 	LATB, F, ACCESS   
		ANDWF 	TRISB, F, ACCESS
	RETURN 0

CDC_Idle: ; and connected...
			; you shouldn't do any blocking here
;		BTG LATB, 2
	RETURN


CDC_RX_char_received:
		; change 3 leds according to ascii code
		MOVF	input_buffer, W
		ANDLW 	0x07
		MOVWF	LATB
		
		; echo char+1
		MOVF	input_buffer, W
		INCF	WREG
		MOVWF	output_buffer
		
		CALL	CDC_IsTxTrfReady ;
		TSTFSZ	WREG
		BRA		CDC_TX_output_buffer
	RETURN
		
CDC_TX_output_buffer:
		MOVLB 	0   
		MOVLW 	output_buffer   
		MOVWF 	pCDCSrc, BANKED   
		CLRF  	pCDCSrcMSB, BANKED   
		MOVLW 	0x1  ; buffer length 
		MOVWF 	cdc_tx_len, BANKED   
		CLRF 	cdc_mem_type, BANKED   
		MOVWF 	cdc_trf_state, BANKED   
	RETURN 0 


CDC_IsTxTrfReady
		MOVLB 	0   
		TSTFSZ 	cdc_trf_state, BANKED
		RETLW	0	; NOT READY
		RETLW	1	; READY

	END

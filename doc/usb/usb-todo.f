( -*- forth -*- )

load (purrr usb-records.f)

( overal design

i'm chopping the message routing into several layers.

layer 0:  pic interrupt level
layer 1:  usb transfer requests
layer 2:  usb requests

)

( layer 1 : USB transfer request )


( questions:
* how to set up data buffers?
)

( variable usb-address )

macro
: configured? usb-state config-state high? ;

( bit field access: byte -- bits )
: 3:1 0x0F and ;    
: 6:5 nswap rot> 0x3 and ;
: 5:2 rot> rot> 0xF and ;
    
( get IN/OUT buffer descriptor struct
layout:
4 bytes EP0 OUT
4 bytes EP0 IN
4 bytes EP1 OUT ... )

( buffer -- index )
: input    << 1 + << << ;    
: output   << << << ;

( load a register with data buffer pointer )
( index -- )    
: data>a   2 + 4 a!! follow ;

forth



: bad-request
    ( illegal request on EP0, set stall bit )
    UEP0 EPSTALL high ;  

: reply-ep0
    ( send a reply packet: data n -- )
    0 xor nz? if
	0 input data>a   ( get EP0 IN buffer address in a register )
	dup 1- al +!     ( last byte in buffer )
	for !a- next     ( fill buffer )
    then
    BD0IBC !         ( set byte count )
    0xC8 BD0IST ! ;  ( send packet as DATA1, set UOWN bit )


    

: get-descriptor
    ( main setup database query.
    accessed by type,index )
    ;


    
: get-configuration
    ( support only one configuration )
    configured? if 1 else 0 then
    1 reply-ep0 ;

: get-interface
    ( no alternate settings )
    1 0 reply-ep0 ;


    

: get-device-status
    configured? if
	device-status @
	0 2 reply-ep0 ;
    then bad-request ;
    
: get-interface-status
    configured? if
	( only of index < nb interfaces )
	0 0 2 reply-ep0 ;
    then bad-request ;

: get-endpoint-status ;
    
: set-address
    value @ address-pending !
    0 reply-ep0 ;

: set-configuration
    ( switch between configured and access state )
    value @ case
	0 of access-state usb-state ! endof
	1 of config-state usb-state ! endof
	bad-request ;
    endcase ;

: set-interface
    config? if
	index @ value @ or z? if
	    0 reply-ep0 ;
	then
    then bad-request ;
    

( STANDARD request dispatcher. this mainly separates the
  good from the bad and the ugly )
: standard-request
    requst-type @ 1:0 case
	DEVICE of
	    request @ case
		SET_ADDRESS       of set-address ; endof
		SET_DESCRIPTOR    of bad-request ; endof
		SET_CONFIGURATION of set-configuration ; endof
		SET_FEATURE       of endof
		CLEAR_FEATURE     of endof

		GET_STATUS        of get-device-status ; endof
		GET_CONFIGURATION of get-configuration ; endof
		GET_DESCRIPTOR    of get-descriptor ; endof 
		bad-request
	    endof
	endof
	INTERFACE of
	    request @ case
		SET_INTERFACE     of set-interface ; endof
		SET_FEATURE       of endof
		CLEAR_FEATURE     of endof

		GET_STATUS        of get-interface-status ; endof
		GET_INTERFACE     of get-interface ; endof
		bad-request
	    endof
	endof
	ENDPOINT of
	    request @ case
		SET_FEATURE       of endof
		CLEAR_FEATURE     of endof

		GET_STATUS        of get-endpoint-status ; endof
		SYNCH_FRAME       of bad-request ; endof
		bad-request
	    endof
	endof
	bad-request
    endcase ;

    
( count -- )
: interpret-setup
    drop  ( it's 8 )

    ( copy SETUP record if necessary )
    
    0x8 dup BD0OBC !  ( reset byte count )
    BD0IST !          ( return IN buffer ownership to us,
                        dequeue pending requests )
    
    setup-request-type @ ( route request )
    6:5 case
	STANDARD of standard-request ; endof
	CLASS    of class-request ; endof
	VENDOR   of vendor-requst ; endof
	bad-request
    endcase
    


( get buffer descriptor and route during the following, the a register
  will point to data buffer )

: interpret-pid
    0x4 ah ! USTAT al ! ( get buffer descriptor address )

    BD status @ >x      ( get status byte )
    BD size @           ( get byte count, ignore high bits )
    BD addr-lo @ follow ( get USB packet buffer address in a )
    
    x> 5:2 case
	IN    of interpret-in ; endof
	OUT   of interpret-out ; endof
	SETUP of interpret-setup ; endof
	error-token
    endcase ;

    
: transfer
    process-token
    UIR TRNIF low    ( ack transfer done )
    ;




( second attempt to router. been thinking. it would be nice to be able
  to compile the backbone from some higlevel description, but in the
  end, it's not insurmountable. having just symbol names and a true
  'case' statement might be good enough. )



: interpret-pid

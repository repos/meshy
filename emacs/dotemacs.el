;; in case you don't have a .emacs use this one
;; running BROOD from scratch

(add-to-list 'load-path
	     "~/brood/emacs") ;; replace this with yours

(require 'cat)
(require 'purrr)


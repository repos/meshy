;; minimal interface to the CAT language

(module cat mzscheme
  (require "composite.ss"   ;; highlevel code compositing
           "base.ss"        ;; base functionality
           "rpn-eval.ss"    ;; run time compilation
           )

  (provide base:) 

  ;; Register for runtime evaluation.

  (rpn-modules "base.ss")

  
  )
(module documentation mzscheme
  (provide documentation
           print-doc)


  (require "ns.ss"
           "list-utils.ss")
  
  ;; (require-for-template "ns.ss")

  (define-syntax documentation
    (syntax-rules ()
      ((_ (ns ...) (name . entry) ...)
       (begin
         (ns-set! '(ns ... name)
                  '(name . entry)) ...))))


  ;; Parser for ad-hoc doc syntax.
  (define (doc->words/stack/rest doc)
    (if (symbol? (second doc))
        (values
         (list (first doc) (second doc))
         (third doc)
         (cdddr doc))
        (values
         (list (first doc))
         (second doc)
         (cddr doc))))

  (define (print-doc word)
    (let ((doc
           (let next ((sub (ns-ls '(doc))))
             (if (null? sub)
                 #f
                 (ns-ref `(doc ,(car sub) ,word)
                         (lambda () (next (cdr sub))))))))
      (if doc
          (let-values
              (((word+arg
                 stack
                 comment)
                (doc->words/stack/rest doc)))
            (for-each
             (lambda (x) (printf "~a " x))
             word+arg)
            (printf "~a " stack)
            (apply printf comment))
          (printf "~a not documented" word))
      (newline)))
  
  )
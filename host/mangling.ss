;; Using the rpn language without run-time namespaces, but using name mangling instead.


(module mangling mzscheme


  (require "rpn-module.ss")

  (require-for-syntax "tx-utils.ss")
  
 
  (module-base-stx module: rpn-)
           
  (prefix-define rpn- + (lambda (b a . s) (cons (+ a b) s)))
  (prefix-define rpn- - (lambda (b a . s) (cons (- a b) s)))
  
  
  

  )
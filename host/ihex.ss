;; intel hex format writing.

;; the basic idea of an ihex file is: 16 bytes per line, and each line
;; has a checksum.


(module ihex mzscheme


  (require
   (lib "match.ss")

   "list-utils.ss"
   "tree-utils.ss"
   "binary-utils.ss")

  (provide (all-defined)) ;; utility
  
  ;; split a list of words into parts.
  ;; (left right) = (0 n)  little endian
  ;;              = (n 0)  big endian
  ;; (split-nibble-list '(1 2 3 4) 0 3)

  (define (split-nibble-list lst left right)
    (let ((mask (make-mask (max left right))))
      (flatten
       (map
        (lambda (x)
          (list (mask (>>> x left))
                (mask (>>> x right))))
        lst))))


  ;; (post) inverse  of above
  ;; (join-nibble-list '(1 1 2 2) 0 8)
  (define (join-nibble-list lst left right)
    (if (= 1 (bitwise-and 1 (length lst)))
        (error 'odd-list-length "join-nibble-list: odd list length: ~a" lst)
        (let
            ((mask
              (make-mask (max left right)))
             (select
              (lambda (lst which)
                (let rest ((l lst))
                  (if (null? l) l
                      (cons (which l)
                            (rest (cddr l))))))))
          (map
           (lambda (l r)
             (bior (<<< (mask l) left)
                   (<<< (mask r) right)))
           
           (select lst first)
           (select lst second)))))




  ;; (hexdigit 15)
  (define hexdigit
    (let ((table
           (apply vector
                  (string->list "0123456789ABCDEF"))))
      (lambda (x)
        (vector-ref table x))))

  ;; (bytes->hexdigits '(1 2 3 255 240))
  (define (bytes->hexdigits lst)
    (map hexdigit (split-nibble-list lst 4 0)))


  ;; (byte->string 123)
  (define (byte->string x)
    (list->string
     (bytes->hexdigits `(,x))))

  ;; (word->string 1123)
  (define (word->string x)
    (list->string
     (bytes->hexdigits
      (split-nibble-list
       `(,x) 8 0))))


  ;; (checksum '(1 2 200 200))
  (define checksum
    (let ((mask (make-mask 8)))
      (lambda (lst)
        (mask (* -1 (fold + 0 lst))))))

  ;; split a 16 bit address in 2 bytes: always big endian
  (define (ihex-split-address address)
    (split-nibble-list `(,address) 8 0))

  ;; create one ihex line
  ;; (ihex-line-list 4 1000 '(1 2 3 4 5 6))
  (define (ihex-line-list type address bytes)
    (let ((line
           `(,(length bytes)
             ,@(ihex-split-address address)
             ,type
             ,@bytes)))
      (append line `(,(checksum line)))))

  ;; (ihex-line-string (ihex-line-list 4 1000 '(1 2 3 4 5)))
  (define (ihex-line-string lst)
    (list->string (append
                   '(#\:)
                   (bytes->hexdigits lst)
                   '(#\return #\linefeed))))

  ;; (ihex-line 4 1000 '(1 2 3))
  (define (ihex-line . args)
    (ihex-line-string
     (apply ihex-line-list args)))

  ;; (display (ihex (sequence (lambda (x) (random 256)) 1 1000) 16 #xaab0))
  (define (ihex-chunk bytes chunksize address)
    (let next ((in   (list->table bytes chunksize))
               (out  '())
               (addr address))

      (match in
             (()
              (apply string-append
                     `(;; address high word
                       ,(ihex-line 4 0 (ihex-split-address
                                        (>>> address 16)))
                       ;; body
                       ,@(reverse out))))

             ;; accumulate body, increment address
             ((line . rest)
              (next rest
                    (cons
                     (ihex-line 0 addr line)
                     out)
                    (+ addr chunksize))))))

  (define (ihex-done)
    (ihex-line 1 0 '()))


  ;; (collect-first number? '(1 2 3 a b c))
  (define (collect-first p? lst)
    (let next ((in  lst)
               (out '()))
      (match in
             ((or ()
                  ((= p? #f) . r))
              (cons (reverse! out) in))

             (((and n (= p? #t)) . r)
              (next r (cons n out))))))


  ;; (bin->chunks '((org 123) 1 2 3 (org 456) 4 5 6))
  (define (bin->chunks binary-lst)
    (let next ((lst binary-lst))
      (match lst
             (() ())
             ((('org addr) . bin)
              (match (collect-first number? bin)
                     ((chunk . rest)
                      (cons `(,addr ,chunk)
                            (next rest)))))
             (else
              (error 'asm-error
                     "bin->chunks: cannot chunk input: ~a"
                     lst)))))
  
  (define (bin? lst)
    (match lst
           (() #t)
           ((('org . x) . y) (bin? y))
           ((x . y) (and (number? x)
                         (bin? y)))))
            

  ;; (align-front '(7 ()))
  ;; (align-back  '(0 (1 2 3)))
  ;; (align-chunk '(3 (1 2 3)) 3)
  ;; (define mask (make-mask 3))

  (define (chunk-align-bits lst bits)
    (let ((mask (make-mask bits)))
      (define (align-front lst)
        (let next ((l lst))
          (match l
                 ((addr r)
                  (if (= 0 (mask addr))
                      l
                      (next `(,(- addr 1)
                              (-1 ,@r)))))))) ;; pad
      (define (align-back lst)
        (match lst
               ((addr numbers)
                (let next ((l (reverse numbers)))
                  (if (= 0 (mask (length l)))
                      `(,addr ,(reverse l))
                      (next (cons -1 l)))))))

      ;; perform front first: back only checks the size
      (align-back
       (align-front lst))))

  
  ;; (split-chunk '(0 (1 2 3 4)) 0 8)
  (define (split-chunk  chunk left right) ;;
    (match chunk
           ((addr things)
            `(,(<<< addr 1)
              ,(split-nibble-list things left right)))))
  
  



  (define (chunks->ihex lst)
    (append
     (map
      (match-lambda
       ((addr code)
        (ihex-chunk code 16 addr)))
      lst)
     `(,(ihex-done))))
  


  ;; terminology:
  ;; a chunk =  (addr things)    can be bytes / words
  


)  
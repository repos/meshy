#!/usr/bin/mzscheme -mqr

;; this script should be invoked in the toplevel brood directory, with
;; as first argument the state file.

;; invoking from emacs/comint as:
;; mzscheme -mqr <script> <state>


;; compile
;; do this in shell script
;(printf "compiling core\n")
;(require (lib "cm.ss"))
;(managed-compile-zo "brood/pic18.ss")

;; args

(define (arg/default index default)
  (if (> (vector-length argv) index)
      (vector-ref argv index)
      default))

(define state (arg/default 0 "prj/purrr18/monitor.state"))

;; load

(printf "loading pic18 compiler core\n")
(require (file "brood/pic18.ss"))
(require (file "brood/catlib.ss"))

(printf "loading ~a\n" state)


;; fall into badnop console loop
(run-source '(badnop-console) `(,state)))

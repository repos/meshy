;; loader module: load cat in a scheme app without name space pollution

(module cat mzscheme

  ;; require all files that define cat words
  (require "catlib.ss")

  (provide (all-defined))

  (define (cat-repl stack)
    (run-source '(go) stack)))




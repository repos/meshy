\ -*- forth -*-

\ this is an example forth file for the 18f1220 chip with serial
\ connection.




\ config bits
#x300000 org

  #x00 , #x26 , #x0F , #x0E ,  
  #x00 , #x01 , #x81 , #x00 ,
  #x0F , #xC0 , #x0F , #xA0 ,  \ B: boot write protect
  #x0F , #x40 , 


macro

\ time
: fosc 40 MHz ;
: baud 38400 ;

\ memory
: stack-data    #x80 ;
: stack-control #x90 ;
: allot-ram     #x00 ;
: allot-flash   #x240 ;  \ reserve 1 erase block for isr code
: read-buffer   #xA0 ;   \ default for terminal.f

forth
  
load p18f452.f          \ chip macros
load monitor-serial.f   \ boot: shared serial monitor/interpeter code


: blink
    0 TRISB !
    #x0F LATB !
    0 for 0 for next next
    #x00 LATB ! ;
  
: init-all-but-rs
    init-ds         \ data and aux/control stack
    init-xs
    init-chip ;     \ setup outputs and clock

    
macro    
: init-all    
    init-rs           \ setup RS needs to be macro
    init-all-but-rs   \ can be shared for alternative reset vector
    init-serial       \ init serial port RX/TX logic    
    ;


    
forth
    
: hello
    fsymbol (::)

: warm
    init-all
    \ debug-transmit
    \ debug-loopback

    blink
    
    debug-serial? if
	interpreter ; 
    then
        application ; 


allot-flash org


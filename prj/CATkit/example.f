  
\ synth test    
: alarm1
    begin
	square
	wait-note

	silence
	3 for wait-note next

	rx-ready?
    until ;


: onetwo
    p0@ 0 ad@ + #x3F and p0
    p1@ 1 ad@ + #x3F and p1 ;
    
: alarm2
    begin
	
	onetwo

	wait-control
	rx-ready?
    until ;


: alarm3
    begin
	onetwo
	6 sync-tick

	S1? if square then
	S2? if xmod2 then
	S3? if xmod3 then
	
	rx-ready?
    until ;


: bar
    begin
	0 ad@ p0
	wait-note 10 burst
	rx-ready?
    until ;
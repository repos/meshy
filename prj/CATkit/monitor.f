\ -*- forth -*-

\ this is an example forth file for the 18f1220 chip with serial
\ connection.


\ config bits
\ internal oscillator, watchdog disabled
#x300000 org
 #x00 , #xC8 , #x0F , #x00 ,  \ 0 1 2 3
 #x00 , #x80 , #x80 , #x00 ,  \ 4 5 6 7
 #x03 , #xC0 , #x03 , #xA0 ,  \ 8 9 A B    \ B : boot protect on / off = #xA0 / #xE0
 #x03 , #x40 ,                \ C D

macro

\ time
: fosc 8000000 ;
: baud 9600 ;

\ memory
: stack-data    #x80 ;
: stack-control #x90 ;
: allot-ram     #x00 ;
: app-boot      #x200 ;    
: allot-flash   #x240 ;  \ reserve 1 block for isr code
: read-buffer   #xA0 ;   \ default for terminal.f
forth

  
load p18f1220.f         \ chip macros
load monitor-serial.f   \ boot: shared serial monitor/interpeter code

: init-all-but-rs
    init-ds         \ data and aux/control stack
    init-xs
    init-chip ;     \ setup outputs and clock

    
macro    
: init-all    
    init-rs           \ setup RS needs to be macro
    init-all-but-rs   \ can be shared for alternative reset vector
    init-serial       \ init serial port RX/TX logic    
    ;


    
forth
    
: hello
    fsymbol CATkit

: warm
    init-all
    \ debug-transmit
    \ debug-loopback

    debug-serial? if
	interpreter ; 
    then
        application ; 


allot-flash org


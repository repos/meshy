\ Some simple self-similar series implementation

\ restart when chip is stuck
: restart
    init-board
    engine-on
    ;

\ basic fixed serie
\ n -> (n  n+1)

    \ 0
    \ 0 1
    \ 0 1 1 2
    \ 0 1 1 2 1 2 2 3
    \ 0 1 1 2 1 2 2 3 1 2 2 3 2 3 3 4

: addup dup 1 + ;

    
\ variables need to be in a word    
variable list-size
variable counter
: pooo 1 counter ! ;
: poo 1 list-size ! ;

\ : dupadd dup 1 + ;

: update-list-size  list-size @ 1 + list-size ! ;
: update-counter counter @ 1 + counter ! ;

: process
    list-size @ counter @ << = if ;
    then update-list-size process ;
    
: count
    counter @ 4 = if ;
    then process update-counter count ;


: cheese
    1 counter !
    1 list-size !
    0 count
    ;
    
	\ : word
	\         if 123 ; then word ;
    
\ can't work, for ... next uses the x stack    
\ : count
\    list-size @
\    for dupadd >x >x next
\    list-size @
\    for x> x> next
\    update-size
\    ;

\ : play  note0 square wait-note silence ;  
    
\ : self 0 count play play ;
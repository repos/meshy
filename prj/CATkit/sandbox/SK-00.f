\ SK-00.f
\ synths sounds

\ Some handy shortcuts
\ By default, when you use "restart", the oct is reset to 3

: set-octave oct ! ;

: c   0 note0 ;
: c#  1 note0 ;
: d   2 note0 ;
: d#  3 note0 ;
: e   4 note0 ;
: f   5 note0 ;
: f#  6 note0 ;
: g   7 note0 ;
: g#  8 note0 ;
: a   9 note0 ;
: a# 10 note0 ;
: b  11 note0 ;

\ STATIC SOUNDS
    
: sq1
    sound ->
    square
    6 for mod-sync next
    silence tail

: xq1
    sound ->
    xmod2
    6 for mod-sync next
    silence tail

variable pw    
: pm1
    sound ->
    pw @ pwm
    12 for mod-sync next
    silence tail

\ DYNAMIC SOUNDS

: ld1
    sound ->    
    1 ad@ pwm \ knob 2 controls the pulse width
    2 for
	2 for p0@ << p0 fast-sync next
	2 for p0@ >> p0 fast-sync next
    next
    tail
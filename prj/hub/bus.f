\ E2 BUS CONTROL
\ fixed for 18f2620: uses RB0 because it serves as INT0


\ to prevent glitches: first perform tristate switch, then set LAT to
\ low.

variable bus-select

\ macro
\ : bus-input  TRISB 0 high ;
\ : bus-output TRISB 0 low ;
\ : bus-high   LATB  0 high ;   
\ : bus-low    LATB  0 low ;
\ forth  

macro
: invert     #xFF xor ;
\ : >c         STATUS ! ;
    
\ : bus-input  TRISB 0 high ;
\ : bus-output TRISB 0 low ;
\ : bus-high   LATB  0 high ;   
\ : bus-low    LATB  0 low ;
forth  
  

: bus-pow
    bus-select @
    dup invert TRISB and!
               LATB or! ;

: bus-power-delay
    10 for 0 for 0 for next next next ;
    

: bus-power-up   -1 LATB ! 0 TRISB ! ; \ power up ports
: bus-power-down  0 LATB ! 0 TRISB ! ;

\ Reset all devices on bus. The timing depends on the value of the
\ capacitors. Note that for correct operations, the slaves should have
\ brown-out enabled to some sane value, i.e. 2V. The reset needs to be
\ executed with rtx disabled.
    
: bus-reset
    bus-power-down
    bus-power-delay
    bus-power-up
    bus-power-delay ;
    
    
: bus-client
     #x80 swap 1 +   \ 0 for = #x100 for
     for rot<< next
     bus-select ! ;

: bus-Z   dup drop-bus-Z ;
: bus-0   dup drop-bus-0 ;

    
\ these are a little bit optimized..
    
: bus-write \ bit --
    dup c! c? if drop-bus-Z ; then
: drop-bus-0
    drop
    bus-select @ invert
    dup TRISB and!
        LATB  and! ;
: drop-bus-Z
    drop
    bus-select @
        TRISB or! ;

\ needs to be in high-Z mode
    
: bus-read \ -- bit
    PORTB @ bus-select @ and
    z? if drop 0 ; then
          drop 1 ;

	

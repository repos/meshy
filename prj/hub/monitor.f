\ -*- forth -*-

\ this is an example forth file for the 18f1220 chip with serial
\ connection.


\ config bits
\ internal oscillator, watchdog disabled

macro

: config-internal-osc
  #x300000 org
    #x00 , #xC8 , #x08 , #x00 ,  \ 0 1 2 3
    #x00 , #x80 , #x80 , #x00 ,  \ 4 5 6 7
    #x03 , #xC0 , #x03 , #xE0 ,  \ 8 9 A B    \ B : boot protect on / off = #xA0 / #xE0
    #x03 , #x40 , ;              \ C D

    

\ time
: fosc 8000000 ;
: baud 9600 ;

\ memory
: stack-data    #x80 ;
: stack-control #x90 ;
: allot-ram     #x00 ;
: app-boot      #x200 ;
: allot-flash   #x240 ;         \ reserve 1 block for isr code FIXME: boot block is larger !

forth

  
config-internal-osc     \ compile config bits
  
load p18f2620.f         \ chip macros
load monitor-serial.f   \ boot: shared serial monitor/interpeter code


: init-all-but-rs
    init-ds                 \ data and aux/control stack
    init-xs
    init-chip ;             \ setup outputs and clock
    
macro    
: init-all    
    init-rs           \ setup RS needs to be macro
    init-all-but-rs   \ can be shared for alternative reset vector
    init-serial       \ init serial port RX/TX logic    
    ;


    
forth
    
: hello
    fsymbol hub

: warm
    init-all

    \ For the hub we can't use the presence of the debug cable to
    \ determine wether the app (hub) should go into debug
    \ mode. However, we do need a way to switch between debugging the
    \ hub and running it, where it runs a _different_ interpreter that
    \ knows about interpreter hubbing. So let's pick a pin here,
    \ arbitrarily, one that might be the debugger's single "big red
    \ button" later.

    PORTB 7 low? 
    \ debug-serial?
    if
        interpreter
    then
        application ;  \ debug transmit ?

\ load hub.f    

allot-flash org


    
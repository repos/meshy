\     
\ PULSE MODULATOR + MULTIPLEXING

\ The framed bit stream above is modulated using the following 4 -
\ phase scheme:
\
\   01XX
\
\ Which is a guaranteed sync (0->1) / power (1 at least 25%) sequence,
\ followed by a 1-bit payload, to be sampled in the middle. Phase
\ starts with 0 so we can start a new cycle from an idle (high) line.

\ The state machine uses even states for TX and odd states for
\ RX. That way state update is 2 +

variable e2-rtx  \ 3 bits : 4 phases x 2 modes (RX/TX)
: e2-rtx+
    e2-rtx @ 7 and
    e2-rtx 1+!
    e2-rtx 1+!
;    

: switch-rx/tx 

    \ If there's a transfer, just leave it.

    rx/tx-busy? if ; then
    
    \ If no transfer is going on, start TX if there's data, otherwise
    \ RX. This might collide with a following incoming byte (no
    \ collision detection), so use a proper software handshaking
    \ protocol.
    
    tx-ready? if
	e2-rtx 0 low ;   \ TX (left column)
    then
        e2-rtx 0 high ;  \ RX (right column)


    
macro
: e2-machine  e2-rtx+ route ;
: e2-next-rx! << 1 + e2-rtx ! ;
: e2-next-tx! << e2-rtx ! ;        
forth  

\ a MASTER machine then looks like:
\
\   : e2-next
\       e2-machine
\
\ 	\ TX      \ RX
\ 	line-0  ; line-0  ;
\ 	line-1  ; line-1  ;
\ 	tx-bit  ;         ;
\ 	        ; rx-bit  ;
\
\ with line-1 calling rx/tx AFTER setting the line high.
  
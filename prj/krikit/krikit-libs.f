load ad.f
load interrupts.f
load table.f
load modem-tables.f  \ the 'cosine-table-xxx' macro
load analog.f
load cos8-fractional.f
load mul8-fractional.f
load conditionals.f
load lfsr.f
load double-math.f
load mul16-global.f
load modulator.f
load demodulator.f
load interface.f
load rgb-led.f
load beep.f


: init-rx/tx
    init-audio
    init-tx
    init-rx ;

: init
    init-rx/tx
    init-rand
    init-rgb
    ;




\ default receive/transmit settings

: rx-mode
    init-rx/tx
    init-rx-timer ;

: tx-mode
    init-rx/tx
    init-tx-timer ;

: rx/valid? \ byte valid --
    rx-mode
    rx-ook-byte ;

: pause
    0 for 0 for next next ;
    
: blink
    pause
    black ;
    
: rx
    rx/valid?
    if
        green blink
        ; then
    drop \ discard garbage
    red blink

    \ necessary to make it recover.. maybe ring from led flash spike?
    2 silence-symbols 
    rx ;
    
: tx
    tx-mode
    ramp-on
    frame-byte
    ramp-off
    
    \ pause is necessary to prevent interference with immediately
    \ following receive.

    8 silence-symbols
    
    ;

: main
: demo
    init
    rx-mode
    \ wait for init spike to die
    8 silence-symbols
    begin
        rx
        tx
    again 

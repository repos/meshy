\ *** NETWORK ***
\ these are from prj/hub

\ FRAMED BUFFERED RX/TX
load frame.f
    
\ ISR
load interrupts.f

\ PULSE MODULATOR + MULTIPLEXING
load e2.f    


\ BUS CONTROL

\ For slave operation, there are only 2 bus states, and a single port.

: bus-init  TRISB 0 high LATB 0 low ;  \ so we can switch 0 <-> Z
: bus-0     TRISB 0 low ;  \ pull down
: bus-Z     TRISB 0 high ; \ release

: bus-write \ bit --
    1 and TRISB @ #xFE and or TRISB ! ;

\ Requires bus to be in Z state !    
: bus-read \ -- bit
    PORTB @ 1 and ;



\ buffer read/write clobbers the a reg, so save it here.

macro
: tx-debug-1 ;
: tx-debug-0 ;
forth

: tx-bit  tx-next bus-write ;
: rx-bit  bus-read rx-next  ;

: bus-Z|wait
    bus-Z
    tmr2-off ;

: tmr2|sw
    8 TMR2 !  \ reset timer: COMPENSATE !
    tmr2-on
    switch-rx/tx ;
    

\ Machine control: in phase 0 the timer is switched off: we're only to
\ be woken up by a 0->1 transition, after which the timer is switched
\ back on to clock the next 3 phases.

variable counter
    
: e2-next
    \ counter 1+! ;
    
    e2-machine

        \ TX       \ RX            \ phase
        bus-Z|wait ; bus-Z|wait ;   \ 0   (release bus + switch off timer)
	tmr2|sw    ; tmr2|sw    ;   \ 1   (interrupt sync)
	tx-bit     ;            ;   \ 2
                   ; rx-bit     ;   \ 3



\ *** TEST & BOOT ***


: machines-init
    init-rx/tx     \ framed byte state machine
\    12 tx-delay !  \ DEBUG
    
    interrupt-init \ config global interrupt stuff
    bus-init       \ enable open drain (0-Z) switching
    bus-Z          \ state = Z  (before int0 init to avoid glitch)
    tmr2-init
    int0-init

    1 e2-next-tx!  \ init E2 state machine (in rx it doesnt work...)

    ;

: e2-sync
    int0-if low
    begin int0-if high? until
    int0-if low ;

    
: e2-isr-on
    cli
    machines-init         \ all config
    \ 10 for e2-sync next   \ wait for a couple of cycles
    tmr2-off
    int0-on               \ waiting for int0
    sti
    ;


: e2-isr-off
    tmr2-off
    int0-off ;
    
: panic cli ;    

: bounce
    begin
        wait-rx-ready
        rx> dup >tx >tx
    again

\ current monitor I/O is determined by this dispatcher
load vector.f
load execute.f   
    
2variable stdio
: stdio-method stdio invoke ;    

: receive     0 stdio-method ;
: transmit    1 stdio-method ;
: console-on  2 stdio-method ;
: console-off 3 stdio-method ;

: e2-isr-rx  wait-rx-ready rx> ;
: e2-isr-tx  wait-tx-room  >tx ;
  
: e2-stdio
    stdio ->
    route
        e2-isr-rx ; e2-isr-tx ;
        e2-isr-on ; e2-isr-off ;

\ : e2-poll stdio ->
\    route
\        e2-rx-poll ; e2-tx-poll ;
\        on ; off ; \ FIXME: no interrupt enable
        
\ run state machine by polling the interrupt flags.
macro
: poll/dispatch iflag |
    iflag compile high? if
        iflag compile low
        e2-next
    then ;
forth      

\ sequencing of the polling interpreter: every 'receive' and 'send'
\ operation will.
  
: polling-interpreter
    begin
        ' tmr2-if poll/dispatch
        ' int0-if poll/dispatch
    again


\ Reload the interpreter bound to the new dynamic stdin/stdout
\ words. This works as long as this core code is compiled separately
\ from the boot loader: names will just be overwritten.

load interpreter.f
    

: e2-isr-init
    e2-stdio      \ connect interpreter to interrupt driven e2 transceiver
    e2-isr-on  ;  \ init + switch on isr rtx
        
: main
    e2-isr-init
\    bounce ;
    
    interpreter ;


macro  
: install
    #x0200 org-push main exit org-pop
    #x0208 org-push

    ' tmr2-if ' e2-next ack/dispatch
    ' int0-if ' e2-next ack/dispatch
    reset \ spurious interrupt

    org-pop ;
  
forth

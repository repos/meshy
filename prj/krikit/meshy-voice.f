\ meshy-voice.f

\ DEBUG
\ : read-meme randbyte ;
: read-meme my-meme @ ;

\ in tx-mode this plays the synth during a millisecond.
: ms for 78 for modulator-tick next next ;

: pause \ in ms
    init-tx-phase 0 cf 0 mf
    ms ;

\ simplified single byte access to carrier and modulator frequency.
: cf  tx-carrier 3 + ! ; \ fast
: mf  envelope   2 + ! ; \ slow
: vol attenuation ! ; 

\ sets oscillator + modulator freq
variable my-cf
variable my-new-cf
variable my-mf
variable pause-length
variable vocal-repeat
variable voice-length
: st-cf my-cf ! ;

: cf-updown?
    my-new-cf @ my-cf @ < if
	1 my-new-cf +!
    else
	1 my-new-cf -!
    then
    \ drop drop
    ;	
    
: cf-slide
    my-new-cf @ my-cf @ = if
    else
	cf-updown?
    then
    ;
  
\ What about scale shifts ?
    \ something like a final multiplier (1, 2 and 3)
    \ after the table map-cords and before st-cf
: map-cords
    table
    1  , 2  , 3  , 4  ,
    5  , 21 , 8  , 9  ,
    23 , 11 , 12 , 22 ,
    20 , 18 , 16 , 24 ,

: cords map-cords st-cf ;
			
: gene-cords
    \ read-meme
    my-meme @
    
    dup >> >> my-mf !
    dup >> >> >> >> cords
    133 +
    dup >> >> >> >> >> >> 1 + vocal-repeat !
    >> >> >> >> 10 + voice-length !
    ;

: vocal-cords
    100 vol
    my-cf @ cf
    my-mf @ mf
    ;

\ make sure the variable names are not too far fetched or confusing!
: phonem  voice-length @ for 4 ms cf-slide my-new-cf @ cf next ;

: voice
    \ read-offset
    tx-mode
    vocal-cords
    ramp-on
    vocal-repeat @ for phonem my-new-cf @ << <<  pause next
    ramp-off
    8 silence-symbols
    ;

: init-voice
    init
    tx-mode
    1 my-cf !
    2 my-new-cf !
    3 my-mf !
    20 voice-length !
    ;

: test-scale
    for gene-cords voice randbyte pause next
    ;
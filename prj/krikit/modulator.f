

\ we send out a single frequency modulated by a cosine wave (which can
\ have frequency zero.)

\ note on fractional multiplication, used both in the modulator and in
\ the demodulator (downmixer). this is using the s1.6 format since
\ this is the smallest one that can support 1, and will allow factors
\ up to 1.111111

macro
: F #x40 * ;
forth
  

2variable  tx-carrier
2variable  tx-carrier-inc

2variable  envelope
2variable  envelope-inc

variable offset  \ AM modulation offset (0 = suppressed tx-carrier)
variable depth   \ modulation index

\ binary phase shift keying = suppressed tx-carrier AM (4-quadrant)  
: bpsk 
    0.0 F offset !
    1.0 F depth ! ;

\ on-off keying = full 0%-100% modulation AM (2-quadrant)    
: ook 
    0.5 F offset !
    0.5 F depth ! ;
    
    
macro
: phase-inc 2 + ;
  
\ state --
: osc-inc state |
    state phase-inc     @  state      +!
    state phase-inc 1 + @  state 1 +  ++! ;


\ state -- value
: osc-I 1 + @ cos ;   
: osc-Q 1 + @ 64 - cos ;
    
\ state -- value  
: osc  state | 
    state osc-I      \ next oscillator value
    state osc-inc ;  \ update phasor

forth

\ -- value
: modulator
    tx-carrier  osc  \ tx-carrier
    
    envelope osc  \ AM: full  suppressed
    depth @ .*    \      1     0.5
    offset @ +    \      0     0.5

    .* ;



\ after modulation the signal needs to be biased, with minimal
\ bias. this means to add #x40. this gives a signal 00-80
\ inclusive. this is taken as the max amplitude.

: bias  #x40 + ;
    

\ initialize modulator default (9 baud, 610 tx-carrier: ratio = 64)

    
    
: init-tx-phase
    #xC000 lohi tx-carrier 2! \ -PI/2 -> 0  (for smoothness)
    #x0000 lohi envelope 2!   \ just at 0,  (for ease of use, start of transition)
    ;

\ 610Hz = 78.1kHz / 2^7    -> inc = 02:00
: init-tx-carrier
    #x0200 lohi tx-carrier phase-inc 2! ;

\ 19 baud uses an evelope frequency of
\ 9.5Hz = 78.1kHz / 2^13   -> inc = 00:08
: envelope-1  #x0008 lohi envelope phase-inc 2! ;
: envelope-0  #x0000 lohi envelope phase-inc 2! ;

\ get envelope state: 0 = carrier or ON, 1 = -carrier or OFF
: envelope@   envelope 1 + @ rot<< 1 and ;

    
variable attenuation
: modulator-tick
    modulator

    bias
    attenuation @ u.* \ unsigned fractional multiplication
    
    wait-sample
    pwm ;

: init-audio
    init-analog
    \ init-modulator
    pwm-on ;



    
\ OFF = ABSOLUTELY NECESSARY AFTER PLAYING A TONE TO PREVENT A LARGE
\ DC CURRENT FROM FLOWING!

\ These will ramp up/down to half the value in 'attenuation' in a
\ multiple of 64 steps, ramp #x00 <-> #x3F.


variable ramp-state
variable ramp-inc

: ramp
    64 for
        ramp-state @
        attenuation @ u.* pwm

        \ if this is too long, you can hear the single step
        \ transitions, which is probably worse than a single
        \ spread-spectrum click..
        
        1 for wait-sample next
        
        ramp-inc @
        ramp-state +!
    next ;

: ramp-on
    0 ramp-state !
    1 ramp-inc !
    ramp ;

: ramp-off
    #x40 ramp-state !
    -1 ramp-inc !
    ramp
: off
    0 pwm ; 


: bump ramp-on ramp-off ;
    
\ Words enclosed in dashes do not terminate DC properly!

\ Run the oscillator for one symbol duration. Most probably this his
\ spans a transition between two symbol midpoints.
    
: >envelope
    1 and route
        envelope-0 ;  \ keep
        envelope-1 ;  \ transition


        
: -transition- \ bit --
    >envelope   -current-transition-

    \ to solve bandwidth problems, the symbol is extended with a
    \ non-transition region.
    
    envelope-0  -current-transition- 
;    
    
: -current-transition- \ --
    32 for \ one symbol (transition) -> half a period of 9.5Hz osc
        128 for \ one tx-carrier period (610Hz)
            modulator-tick
        next 
    next ;    

\ the envelope is driven with transitions. the word 'envelope@' can be
\ used to send absolute state.
    
: -symbol- \ bit --
    envelope@ xor -transition- ;
    
    
\ send out a number of symbol bytes from an array in ram.    
: ram-bytes \ n --
    ramp-on
    for
        @a+ byte
    next
    ramp-off ;

\ send out a single byte, absolute
: byte \ b --
    8 for
        dup 1 and -symbol-
        rot>>
    next
    drop ;

\ for OOK, 1 = OFF and 0 = ON. this is to respect the convention that
\ 1 is the idle line, which makes sense to choose as carrier=OFF. the
\ end marker is there to distinguish transient sounds that might
\ trigger the reception of a start bit. frame length is a little over
\ a second, so the chance any transient sound can have this symbol ON
\ is very small, which means it can be used to distinguish between
\ machine and human sounds.
    
: frame-byte \ byte --
    0 -symbol-   \ start bit (carrier ON or INVERSE)
    byte
 \  1 -symbol-   \ marker 1            OFF 
    0 -symbol-   \ marker 2            ON
    1 -symbol-   \ stop bit = idle     OFF
    ;
    

: silence-symbols
    for 32 for 128 for wait-sample next next next ;

\ : last-symbols
\     ramp-on
\     for -last-symbol- next
\     ramp-off ;

: -random-symbol-
    0 1 randbits
    -symbol- ;
    
: random-symbols \ n --
    ramp-on
    for  -random-symbol- next
    ramp-off ;
        
: random-loop
    begin -random-symbol- again ;

: init-modulator
    50 attenuation !
    init-tx-phase
    init-tx-carrier
    envelope-1 ;

: init-tx
    init-modulator
    ook ;

\ : send-ram \ n --
\     0 0 a!!
\     ram-bytes ;


: 0-loop
    init-tx
    begin
        0 -symbol- \ carrier
    again ;


: 1-loop
    init-tx
    bpsk
    begin
        1 -symbol- \ carrier reversals
    again ;



    
: ook-numbers
    init
    init-tx ook
    0 \ state
    begin
        ramp-on
        dup frame-byte 1 +
        ramp-off
        64 silence-symbols \ wait for 8 bytes
    again 

: ook-55
    init-tx ook
    begin
        ramp-on
        #x55 frame-byte
        ramp-off
        64 silence-symbols
    again
        
: r-loop
    init-tx
    random-loop ;
    
    
macro
: install
    app-boot org-push main exit org-pop ;
forth

    
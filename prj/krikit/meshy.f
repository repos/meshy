\ meshy.f
load krikit-libs.f
load rgb-led.f

load meshy-color.f
load meshy-sys.f
load meshy-fuzzy.f
load meshy-voice.f

\ TODO
\ - increase fuzzy logic tolerance
\ - either make bounce or meme more green
\ - use another way for initial random meme
\ - startup random meme

: init-meshy
    init-rgb
    init-rand
    init-fuzzy
    init-voice
    init-color
    ;

: invasion
    new-meme !
    decision
    ;

: listen
    rx/valid? 1 = if
	invasion
    else
	drop
	\ red-blink \ DEBUG invalid meme bounced
	led-invalid
    then ;

: meshy-pause 2 silence-symbols ;

: meshy-listen  listen meshy-pause ;
: meshy-meme    gene-cords voice led-meme meshy-pause ;
: meshy-speak   play-meme meshy-pause ;

    
: main    
: meshy
    init-meshy
    begin
	meshy-listen
	meshy-meme meshy-meme
	meshy-listen
	meshy-meme
	meshy-speak
    again
    

\ DEBUG

\ : main
: garbage-station
    init-meshy
    begin
	green-blink
	2 silence-symbols
	randbyte my-meme ! play-meme
	100 for blink-pause next
    again
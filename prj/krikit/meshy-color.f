\ meshy-color.f


: init-color 1 1 1 rgb ;    
    
: add-r 1 d-red +! ;
: sub-r 1 d-red -! ;
: add-g 1 d-green +! ;
: sub-g 1 d-green -! ;
: add-b 1 d-blue +! ;
: sub-b 1 d-blue -! ;
    
: led-pause for led-next led-duty next ;
: nop-pause for led-duty next ;

\ organism info colors
: led-invalid
    127 00 0 rgb 255 led-pause
    127 127 0 rgb 255 led-pause
    127 00 0 rgb 255 led-pause
    127 127 0 rgb 255 led-pause
    black
    ;

: led-bounce
    3 for
	0 160 10 rgb 150 led-pause
	black 50 nop-pause
    next
    0 160 10 rgb
    140 for
	sub-g
	add-b
	10 led-pause
    next
    0 160 10 rgb 150 led-pause
    black
    ;

: led-contamination
    10 10 10 rgb
    117 for
	add-b
	10 led-pause
    next
    127 for
	add-r add-r
	add-g add-g
	add-b
	20 led-pause
    next
    black
    ;

\ meme info colors
: led-meme-pause1 my-meme @ >> >> >> >> 5 + led-pause ;
: led-meme-pause2 my-meme @ >> >> >> >> >> 5 + led-pause ;
    
: led-meme
    0 20 10 rgb
    2 for
	50 for
	    add-b
	    led-meme-pause1 
	next
	50 for
	    add-g add-g add-r
	    led-meme-pause2
	next
	50 for
	    add-b add-r
	    led-meme-pause1
	next
	100 for
	    sub-g sub-b sub-r
	    led-meme-pause2
	next
    next
    black
    ;


\ DEBUG    
: led-meme-test
    for
	genememe
	contaminate
	led-meme
	black 50 nop-pause
    next ;
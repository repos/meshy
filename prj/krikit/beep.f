\ in tx-mode this plays the synth during a millisecond.
: ms for 78 for modulator-tick next next ;

\ sets oscillator + modulator freq
: squeak    
    20 vol
    8 cf
    128 mf
    ;

\ simplified single byte access to carrier and modulator frequency.
: cf  tx-carrier 3 + ! ; \ fast
: mf  envelope   2 + ! ; \ slow
: vol attenuation ! ;   

: cf+ tx-carrier 3 + +! ;
    
    
: kanari \  --

    tx-mode  \ setup for transmit
    squeak   \ set oscillator params
    
    ramp-on  \ bring speaker to DC bias
    
    \ beep for a number of milliseconds
    10 for
        13 ms
        1 cf+ \ increment carrier frequency
    next
    

    ramp-off \ gently shut down speaker DC bias.
;

    
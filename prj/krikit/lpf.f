macro    
: flip  #x80 xor ; \ convert to signed (flip sign bit)

\ The filter pole is taken to be (1 - 2^(-8)) which is easy to
\ implement. The marginal filter frequency is computed as follows. The
\ filter response has to decay below (1/2) in a timespan equal to one
\ symbol length. The upper bound for the parameter p in the filter
\ pole (1 - 2^(-p)) can be approximated by: p = log_2 (2t) for large
\ values of t. In the example we use, t = 512, giving a marginal p =
\ 10, which we replace by 8 to give a simpler algorithm.


: *filter-decay state |
    \ compute filter decay: state -= 2^(-n) * state
    \ n = 9 = 8 + 1
    
    state 1 + @ state -!
    state 2 + @ state 1 + --!
              0 state 2 + --!
    ;
    
: *filter-input state |
    \ perform mix and add: state += 2^(-8) * signal
    >x
       state +!
    
    x> flip     \ convert value to unsigned

       state 1 + ++!
     0 state 2 + ++!
    ;
    
: *filter state | \ sl sh state --
    state *filter-decay
    state *filter-input
    ;

forth


\ The data filter.
  
2variable lpf-I variable lpf-I-headroom
2variable lpf-Q variable lpf-Q-headroom

macro
: 3! lo hi up addr |
    lo addr !
    hi addr 1 + !
    up addr 2 + ! ;
: lohiup
    lohi lohi ;
forth

\ state is stored in 24 unsigned  
: init-lpf
    #x800000 lohiup lpf-I 3!
    #x800000 lohiup lpf-Q 3! ;

\ *** NETWORK ***
\ these are from prj/hub

\ FRAMED BUFFERED RX/TX
load frame.f
    
\ ISR
load interrupts.f

\ BUS CONTROL
load bus.f    
    
\ PULSE MODULATOR + MULTIPLEXING
load rtx.f    


\ buffer read/write clobbers the a reg, so save it here.

macro
: tx-debug-1 ;
: tx-debug-0 ;
forth

: tx-bit  tx-next bus-write ;
: rx-bit  bus-read rx-next  ;

: bus-1|off
    bus-1
    tmr2-off ;

: rtx-on
    8 TMR2 !  \ reset timer: COMPENSATE !
    tmr2-on
    switch-rx/tx ;
    

\ Machine control: in phase 0 the timer is switched off: we're only to
\ be woken up by a 0->1 transition, after which the timer is switched
\ back on to clock the next 3 phases.

variable counter
    
: rtx-next
    \ counter 1+! ;
    
    rtx-machine

        \ TX       \ RX           \ phase
        bus-1|off ; bus-1|off ;   \ 0
	rtx-on    ; rtx-on    ;   \ 1   (interrupt sync)
	tx-bit    ;           ;   \ 2
                  ; rx-bit    ;   \ 3



\ *** TEST & BOOT ***

: machines-init
    init-rx/tx   \ framed byte state machine
    tmr2-init
    int0-init
    bus-1

    init-rtx-tx  \ E2 state machine predictable phase: 1 TX
    int0-on      \ waiting for int0
    ;
    
: on
    machines-init  \ all config
    interrupt-init \ using interrupts
    ;
    \ : off timer-off bus-1 ;   \ means function off (not bus off)

: off
    tmr2-off
    int0-off ;
    
: panic cli ;    

: bounce
    begin
        wait-rx-ready
        rx> >tx
    again

\ current monitor I/O is determined by this dispatcher
load vector.f
load execute.f   
    
2variable stdio
: do-stdio stdio invoke ;    

: receive     0 do-stdio ;
: transmit    1 do-stdio ;
: console-on  2 do-stdio ;
: console-off 3 do-stdio ;

: e2-rx-isr  wait-rx-ready rx> ;
: e2-tx-isr wait-tx-room >tx ;
  
: e2-isr
    on
    stdio ->
    route
        e2-rx-isr ; e2-tx-isr ;
        on ; off ;

\ : e2-poll stdio ->
\    route
\        e2-rx-poll ; e2-tx-poll ;
\        on ; off ; \ FIXME: no interrupt enable
        
\ run state machine by polling the interrupt flags.
macro
: poll/dispatch iflag |
    iflag compile high? if
        iflag compile low
        rtx-next
    then ;
forth      

\ sequencing of the polling interpreter: every 'receive' and 'send'
\ operation will.
  
: polling-interpreter
    begin
        ' tmr2-if poll/dispatch
        ' int0-if poll/dispatch
    again


\ Reload the interpreter bound to the new dynamic stdin/stdout
\ words. This works as long as this core code is compiled separately
\ from the boot loader: names will just be overwritten.

load interpreter.f
    


    
: main
    e2-isr   \ interrupt driven e2 transceiver
    interpreter ;


macro  
: install
    #x0200 org-push main exit org-pop
    #x0208 org-push

    ' tmr2-if ' rtx-next ack/dispatch
    ' int0-if ' rtx-next ack/dispatch
    reset \ spurious interrupt

    org-pop ;
  
forth

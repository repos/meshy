\ -*- forth -*-

\ this is an example forth file for the 18f1220 chip with serial
\ connection.


\ config bits
\ internal oscillator, watchdog disabled

macro

: config-internal-osc
    \ 2 : power-up timer on, brown out detect enabled = 2.0 V
    \ B : boot protect on / off = #xA0 / #xE0
  #x300000 org
    #x00 , #xC8 , #x1E , #x00 ,  \ 0 1 2 3    
    #x00 , #x80 , #x80 , #x00 ,  \ 4 5 6 7
    #x03 , #xC0 , #x03 , #xE0 ,  \ 8 9 A B    
    #x03 , #x40 , ;              \ C D


: config-hspll
    \ 2 : power-up timer on, brown out detect enabled = 2.0 V
    \ B : boot protect on / off = #xA0 / #xE0
  #x300000 org
    #x00 , #xC6 , #x1E , #x00 ,  \ 0 1 2 3    
    #x00 , #x80 , #x80 , #x00 ,  \ 4 5 6 7
    #x03 , #xC0 , #x03 , #xE0 ,  \ 8 9 A B    
    #x03 , #x40 , ;              \ C D

    

\ time
: fosc
    40 MHz
    \ 8 MHz 
    ;
: baud
    38400
    \ 9600
    ;

\ memory
: stack-data    #x80 ;
: stack-control #x90 ;
: allot-ram     #x00 ;
: app-boot      #x800 ;    
: allot-flash   #x840 ;         \ reserve 1 block for isr code FIXME: boot block is larger !

forth

\ compile config bits  
\ config-internal-osc     
config-hspll

  
load p18f2620.f         \ chip macros
load monitor-serial.f   \ boot: shared serial monitor/interpeter code


: init-all-but-rs
    init-ds                 \ data and aux/control stack
    init-xs
    init-chip ;             \ setup outputs and clock
    
macro    
: init-all    
    init-rs           \ setup RS needs to be macro
    init-all-but-rs   \ can be shared for alternative reset vector
    init-serial       \ init serial port RX/TX logic    
    ;


    
forth

macro
: status-init    TRISC 0 low ;
: status-on      LATC 0 high ;
: status-off     LATC 0 low ;
: status-toggle  LATC 0 toggle ;  
: status-wait 0 for 0 for next next ;

forth
  
: status-blink
    status-init
    20 for status-toggle status-wait  next ;
  
    
: hello
    fsymbol krikit

: warm
    init-all
    
    status-blink
    
    debug-serial? if
        interpreter
    then
        application ; 


allot-flash org

